//////////////////////////////////////////
//
// NOTE: This is *not* a valid shader file
//
///////////////////////////////////////////
Shader "Graphy/Graph Mobile" {
Properties {
_MainTex ("Sprite Texture", 2D) = "white" { }
_Color ("Tint", Color) = (1,1,1,1)
[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
_GoodColor ("Good Color", Color) = (1,1,1,1)
_CautionColor ("Caution Color", Color) = (1,1,1,1)
_CriticalColor ("Critical Color", Color) = (1,1,1,1)
_GoodThreshold ("Good Threshold", Float) = 0.5
_CautionThreshold ("Caution Threshold", Float) = 0.25
}
SubShader {
 Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
 Pass {
  Name "Default"
  Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
  Blend One OneMinusSrcAlpha, One OneMinusSrcAlpha
  ZTest Off
  ZWrite Off
  Cull Off
  GpuProgramID 6821
Program "vp" {
SubProgram "gles3 " {
"#ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump vec4 _Color;
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_COLOR0;
out highp vec2 vs_TEXCOORD0;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat0 = in_COLOR0 * _Color;
    vs_COLOR0 = u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 _GoodColor;
uniform 	mediump vec4 _CautionColor;
uniform 	mediump vec4 _CriticalColor;
uniform 	mediump float _GoodThreshold;
uniform 	mediump float _CautionThreshold;
uniform 	float Average;
uniform 	float GraphValues[128];
uniform 	float GraphValues_Length;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
in mediump vec4 vs_COLOR0;
in highp vec2 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
mediump vec4 u_xlat16_0;
mediump vec4 u_xlat16_1;
mediump vec4 u_xlat16_2;
float u_xlat3;
uint u_xlatu3;
bvec2 u_xlatb3;
mediump float u_xlat16_5;
float u_xlat7;
bvec2 u_xlatb7;
float u_xlat11;
bvec2 u_xlatb11;
float u_xlat15;
void main()
{
    u_xlat16_0 = vs_COLOR0 * _GoodColor;
    u_xlat16_1 = vs_COLOR0 * _CautionColor;
    u_xlat16_2 = vs_COLOR0 * _CriticalColor;
    u_xlat3 = vs_TEXCOORD0.x * GraphValues_Length;
    u_xlat3 = floor(u_xlat3);
    u_xlatu3 = uint(u_xlat3);
    u_xlatb7.xy = lessThan(vec4(_GoodThreshold, _CautionThreshold, _GoodThreshold, _GoodThreshold), vec4(GraphValues[int(u_xlatu3)])).xy;
    u_xlat16_1 = (u_xlatb7.y) ? u_xlat16_1 : u_xlat16_2;
    u_xlat16_0 = (u_xlatb7.x) ? u_xlat16_0 : u_xlat16_1;
    u_xlat16_1.x = vs_TEXCOORD0.y * 0.300000012;
    u_xlat7 = u_xlat16_1.x / GraphValues[int(u_xlatu3)];
    u_xlat7 = u_xlat16_0.w * u_xlat7;
    u_xlat11 = (-vs_TEXCOORD0.y) + GraphValues[int(u_xlatu3)];
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(GraphValues[int(u_xlatu3)]<vs_TEXCOORD0.y);
#else
    u_xlatb3.x = GraphValues[int(u_xlatu3)]<vs_TEXCOORD0.y;
#endif
    u_xlat15 = GraphValues_Length + -1.0;
    u_xlat15 = float(1.0) / u_xlat15;
    u_xlat15 = u_xlat15 * 4.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb11.x = !!(u_xlat15<u_xlat11);
#else
    u_xlatb11.x = u_xlat15<u_xlat11;
#endif
    u_xlat16_1.x = (u_xlatb11.x) ? u_xlat7 : u_xlat16_0.w;
    u_xlat16_0.w = (u_xlatb3.x) ? 0.0 : u_xlat16_1.x;
    u_xlat3 = Average + -0.0199999996;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(u_xlat3<vs_TEXCOORD0.y);
#else
    u_xlatb3.x = u_xlat3<vs_TEXCOORD0.y;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb7.x = !!(vs_TEXCOORD0.y<Average);
#else
    u_xlatb7.x = vs_TEXCOORD0.y<Average;
#endif
    u_xlatb3.x = u_xlatb3.x && u_xlatb7.x;
    u_xlat16_0 = (u_xlatb3.x) ? vec4(1.0, 1.0, 1.0, 1.0) : u_xlat16_0;
    u_xlat16_1.xy = vec2(_CautionThreshold, _GoodThreshold) + vec2(-0.0199999996, -0.0199999996);
    u_xlatb3.xy = lessThan(u_xlat16_1.xyxx, vs_TEXCOORD0.yyyy).xy;
    u_xlatb11.xy = lessThan(vs_TEXCOORD0.yyyy, vec4(_CautionThreshold, _GoodThreshold, _CautionThreshold, _GoodThreshold)).xy;
    u_xlatb3.x = u_xlatb3.x && u_xlatb11.x;
    u_xlatb3.y = u_xlatb3.y && u_xlatb11.y;
    u_xlat16_0 = (u_xlatb3.x) ? _CautionColor : u_xlat16_0;
    u_xlat16_0 = (u_xlatb3.y) ? _GoodColor : u_xlat16_0;
    u_xlat16_1.xy = (-vs_TEXCOORD0.xx) + vec2(0.0299999993, 1.0);
    u_xlat16_1.y = u_xlat16_1.y * 33.3333359;
    u_xlat16_1.x = (-u_xlat16_1.x) * 33.3333359 + 1.0;
    u_xlat16_1.xy = u_xlat16_0.ww * u_xlat16_1.xy;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(0.970000029<vs_TEXCOORD0.x);
#else
    u_xlatb3.x = 0.970000029<vs_TEXCOORD0.x;
#endif
    u_xlat16_5 = (u_xlatb3.x) ? u_xlat16_1.y : u_xlat16_0.w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(vs_TEXCOORD0.x<0.0299999993);
#else
    u_xlatb3.x = vs_TEXCOORD0.x<0.0299999993;
#endif
    u_xlat16_0.w = (u_xlatb3.x) ? u_xlat16_1.x : u_xlat16_5;
    u_xlat16_1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat16_0 = u_xlat16_0 * u_xlat16_1;
    SV_Target0.xyz = u_xlat16_0.www * u_xlat16_0.xyz;
    SV_Target0.w = u_xlat16_0.w;
    return;
}

#endif
"
}
SubProgram "vulkan " {
"; SPIR-V
; Version: 1.0
; Generator: Khronos Glslang Reference Front End; 6
; Bound: 101
; Schema: 0
                                                     OpCapability Shader 
                                              %1 = OpExtInstImport "GLSL.std.450" 
                                                     OpMemoryModel Logical GLSL450 
                                                     OpEntryPoint Vertex %4 "main" %11 %72 %82 %87 %91 %93 
                                                     OpName vs_TEXCOORD0 "vs_TEXCOORD0" 
                                                     OpDecorate %11 Location 11 
                                                     OpDecorate %16 ArrayStride 16 
                                                     OpDecorate %17 ArrayStride 17 
                                                     OpMemberDecorate %18 0 Offset 18 
                                                     OpMemberDecorate %18 1 Offset 18 
                                                     OpMemberDecorate %18 2 RelaxedPrecision 
                                                     OpMemberDecorate %18 2 Offset 18 
                                                     OpDecorate %18 Block 
                                                     OpDecorate %20 DescriptorSet 20 
                                                     OpDecorate %20 Binding 20 
                                                     OpMemberDecorate %70 0 BuiltIn 70 
                                                     OpMemberDecorate %70 1 BuiltIn 70 
                                                     OpMemberDecorate %70 2 BuiltIn 70 
                                                     OpDecorate %70 Block 
                                                     OpDecorate %82 Location 82 
                                                     OpDecorate %85 RelaxedPrecision 
                                                     OpDecorate %87 RelaxedPrecision 
                                                     OpDecorate %87 Location 87 
                                                     OpDecorate vs_TEXCOORD0 Location 91 
                                                     OpDecorate %93 Location 93 
                                              %2 = OpTypeVoid 
                                              %3 = OpTypeFunction %2 
                                              %6 = OpTypeFloat 32 
                                              %7 = OpTypeVector %6 4 
                                              %8 = OpTypePointer Private %7 
                               Private f32_4* %9 = OpVariable Private 
                                             %10 = OpTypePointer Input %7 
                                Input f32_4* %11 = OpVariable Input 
                                             %14 = OpTypeInt 32 0 
                                         u32 %15 = OpConstant 4 
                                             %16 = OpTypeArray %7 %15 
                                             %17 = OpTypeArray %7 %15 
                                             %18 = OpTypeStruct %16 %17 %7 
                                             %19 = OpTypePointer Uniform %18 
Uniform struct {f32_4[4]; f32_4[4]; f32_4;}* %20 = OpVariable Uniform 
                                             %21 = OpTypeInt 32 1 
                                         i32 %22 = OpConstant 0 
                                         i32 %23 = OpConstant 1 
                                             %24 = OpTypePointer Uniform %7 
                                         i32 %35 = OpConstant 2 
                                         i32 %44 = OpConstant 3 
                              Private f32_4* %48 = OpVariable Private 
                                         u32 %68 = OpConstant 1 
                                             %69 = OpTypeArray %6 %68 
                                             %70 = OpTypeStruct %7 %6 %69 
                                             %71 = OpTypePointer Output %70 
        Output struct {f32_4; f32; f32[1];}* %72 = OpVariable Output 
                                             %80 = OpTypePointer Output %7 
                                Input f32_4* %82 = OpVariable Input 
                               Output f32_4* %87 = OpVariable Output 
                                             %89 = OpTypeVector %6 2 
                                             %90 = OpTypePointer Output %89 
                      Output f32_2* vs_TEXCOORD0 = OpVariable Output 
                                             %92 = OpTypePointer Input %89 
                                Input f32_2* %93 = OpVariable Input 
                                             %95 = OpTypePointer Output %6 
                                         void %4 = OpFunction None %3 
                                              %5 = OpLabel 
                                       f32_4 %12 = OpLoad %11 
                                       f32_4 %13 = OpVectorShuffle %12 %12 1 1 1 1 
                              Uniform f32_4* %25 = OpAccessChain %20 %22 %23 
                                       f32_4 %26 = OpLoad %25 
                                       f32_4 %27 = OpFMul %13 %26 
                                                     OpStore %9 %27 
                              Uniform f32_4* %28 = OpAccessChain %20 %22 %22 
                                       f32_4 %29 = OpLoad %28 
                                       f32_4 %30 = OpLoad %11 
                                       f32_4 %31 = OpVectorShuffle %30 %30 0 0 0 0 
                                       f32_4 %32 = OpFMul %29 %31 
                                       f32_4 %33 = OpLoad %9 
                                       f32_4 %34 = OpFAdd %32 %33 
                                                     OpStore %9 %34 
                              Uniform f32_4* %36 = OpAccessChain %20 %22 %35 
                                       f32_4 %37 = OpLoad %36 
                                       f32_4 %38 = OpLoad %11 
                                       f32_4 %39 = OpVectorShuffle %38 %38 2 2 2 2 
                                       f32_4 %40 = OpFMul %37 %39 
                                       f32_4 %41 = OpLoad %9 
                                       f32_4 %42 = OpFAdd %40 %41 
                                                     OpStore %9 %42 
                                       f32_4 %43 = OpLoad %9 
                              Uniform f32_4* %45 = OpAccessChain %20 %22 %44 
                                       f32_4 %46 = OpLoad %45 
                                       f32_4 %47 = OpFAdd %43 %46 
                                                     OpStore %9 %47 
                                       f32_4 %49 = OpLoad %9 
                                       f32_4 %50 = OpVectorShuffle %49 %49 1 1 1 1 
                              Uniform f32_4* %51 = OpAccessChain %20 %23 %23 
                                       f32_4 %52 = OpLoad %51 
                                       f32_4 %53 = OpFMul %50 %52 
                                                     OpStore %48 %53 
                              Uniform f32_4* %54 = OpAccessChain %20 %23 %22 
                                       f32_4 %55 = OpLoad %54 
                                       f32_4 %56 = OpLoad %9 
                                       f32_4 %57 = OpVectorShuffle %56 %56 0 0 0 0 
                                       f32_4 %58 = OpFMul %55 %57 
                                       f32_4 %59 = OpLoad %48 
                                       f32_4 %60 = OpFAdd %58 %59 
                                                     OpStore %48 %60 
                              Uniform f32_4* %61 = OpAccessChain %20 %23 %35 
                                       f32_4 %62 = OpLoad %61 
                                       f32_4 %63 = OpLoad %9 
                                       f32_4 %64 = OpVectorShuffle %63 %63 2 2 2 2 
                                       f32_4 %65 = OpFMul %62 %64 
                                       f32_4 %66 = OpLoad %48 
                                       f32_4 %67 = OpFAdd %65 %66 
                                                     OpStore %48 %67 
                              Uniform f32_4* %73 = OpAccessChain %20 %23 %44 
                                       f32_4 %74 = OpLoad %73 
                                       f32_4 %75 = OpLoad %9 
                                       f32_4 %76 = OpVectorShuffle %75 %75 3 3 3 3 
                                       f32_4 %77 = OpFMul %74 %76 
                                       f32_4 %78 = OpLoad %48 
                                       f32_4 %79 = OpFAdd %77 %78 
                               Output f32_4* %81 = OpAccessChain %72 %22 
                                                     OpStore %81 %79 
                                       f32_4 %83 = OpLoad %82 
                              Uniform f32_4* %84 = OpAccessChain %20 %35 
                                       f32_4 %85 = OpLoad %84 
                                       f32_4 %86 = OpFMul %83 %85 
                                                     OpStore %9 %86 
                                       f32_4 %88 = OpLoad %9 
                                                     OpStore %87 %88 
                                       f32_2 %94 = OpLoad %93 
                                                     OpStore vs_TEXCOORD0 %94 
                                 Output f32* %96 = OpAccessChain %72 %22 %68 
                                         f32 %97 = OpLoad %96 
                                         f32 %98 = OpFNegate %97 
                                 Output f32* %99 = OpAccessChain %72 %22 %68 
                                                     OpStore %99 %98 
                                                     OpReturn
                                                     OpFunctionEnd
; SPIR-V
; Version: 1.0
; Generator: Khronos Glslang Reference Front End; 6
; Bound: 348
; Schema: 0
                                                      OpCapability Shader 
                                               %1 = OpExtInstImport "GLSL.std.450" 
                                                      OpMemoryModel Logical GLSL450 
                                                      OpEntryPoint Fragment %4 "main" %11 %41 %334 
                                                      OpExecutionMode %4 OriginUpperLeft 
                                                      OpName vs_TEXCOORD0 "vs_TEXCOORD0" 
                                                      OpDecorate %9 RelaxedPrecision 
                                                      OpDecorate %11 RelaxedPrecision 
                                                      OpDecorate %11 Location 11 
                                                      OpDecorate %12 RelaxedPrecision 
                                                      OpDecorate %15 ArrayStride 15 
                                                      OpMemberDecorate %16 0 RelaxedPrecision 
                                                      OpMemberDecorate %16 0 Offset 16 
                                                      OpMemberDecorate %16 1 RelaxedPrecision 
                                                      OpMemberDecorate %16 1 Offset 16 
                                                      OpMemberDecorate %16 2 RelaxedPrecision 
                                                      OpMemberDecorate %16 2 Offset 16 
                                                      OpMemberDecorate %16 3 RelaxedPrecision 
                                                      OpMemberDecorate %16 3 Offset 16 
                                                      OpMemberDecorate %16 4 RelaxedPrecision 
                                                      OpMemberDecorate %16 4 Offset 16 
                                                      OpMemberDecorate %16 5 Offset 16 
                                                      OpMemberDecorate %16 6 Offset 16 
                                                      OpMemberDecorate %16 7 Offset 16 
                                                      OpDecorate %16 Block 
                                                      OpDecorate %18 DescriptorSet 18 
                                                      OpDecorate %18 Binding 18 
                                                      OpDecorate %23 RelaxedPrecision 
                                                      OpDecorate %24 RelaxedPrecision 
                                                      OpDecorate %25 RelaxedPrecision 
                                                      OpDecorate %26 RelaxedPrecision 
                                                      OpDecorate %29 RelaxedPrecision 
                                                      OpDecorate %30 RelaxedPrecision 
                                                      OpDecorate %31 RelaxedPrecision 
                                                      OpDecorate %32 RelaxedPrecision 
                                                      OpDecorate %35 RelaxedPrecision 
                                                      OpDecorate %36 RelaxedPrecision 
                                                      OpDecorate vs_TEXCOORD0 Location 41 
                                                      OpDecorate %63 RelaxedPrecision 
                                                      OpDecorate %66 RelaxedPrecision 
                                                      OpDecorate %68 RelaxedPrecision 
                                                      OpDecorate %70 RelaxedPrecision 
                                                      OpDecorate %85 RelaxedPrecision 
                                                      OpDecorate %86 RelaxedPrecision 
                                                      OpDecorate %91 RelaxedPrecision 
                                                      OpDecorate %92 RelaxedPrecision 
                                                      OpDecorate %102 RelaxedPrecision 
                                                      OpDecorate %110 RelaxedPrecision 
                                                      OpDecorate %156 RelaxedPrecision 
                                                      OpDecorate %167 RelaxedPrecision 
                                                      OpDecorate %168 RelaxedPrecision 
                                                      OpDecorate %199 RelaxedPrecision 
                                                      OpDecorate %201 RelaxedPrecision 
                                                      OpDecorate %202 RelaxedPrecision 
                                                      OpDecorate %204 RelaxedPrecision 
                                                      OpDecorate %207 RelaxedPrecision 
                                                      OpDecorate %208 RelaxedPrecision 
                                                      OpDecorate %216 RelaxedPrecision 
                                                      OpDecorate %218 RelaxedPrecision 
                                                      OpDecorate %220 RelaxedPrecision 
                                                      OpDecorate %222 RelaxedPrecision 
                                                      OpDecorate %245 RelaxedPrecision 
                                                      OpDecorate %247 RelaxedPrecision 
                                                      OpDecorate %248 RelaxedPrecision 
                                                      OpDecorate %255 RelaxedPrecision 
                                                      OpDecorate %257 RelaxedPrecision 
                                                      OpDecorate %258 RelaxedPrecision 
                                                      OpDecorate %268 RelaxedPrecision 
                                                      OpDecorate %270 RelaxedPrecision 
                                                      OpDecorate %273 RelaxedPrecision 
                                                      OpDecorate %274 RelaxedPrecision 
                                                      OpDecorate %275 RelaxedPrecision 
                                                      OpDecorate %276 RelaxedPrecision 
                                                      OpDecorate %278 RelaxedPrecision 
                                                      OpDecorate %279 RelaxedPrecision 
                                                      OpDecorate %280 RelaxedPrecision 
                                                      OpDecorate %281 RelaxedPrecision 
                                                      OpDecorate %282 RelaxedPrecision 
                                                      OpDecorate %290 RelaxedPrecision 
                                                      OpDecorate %297 RelaxedPrecision 
                                                      OpDecorate %300 RelaxedPrecision 
                                                      OpDecorate %301 RelaxedPrecision 
                                                      OpDecorate %312 RelaxedPrecision 
                                                      OpDecorate %314 RelaxedPrecision 
                                                      OpDecorate %315 RelaxedPrecision 
                                                      OpDecorate %317 RelaxedPrecision 
                                                      OpDecorate %320 RelaxedPrecision 
                                                      OpDecorate %320 DescriptorSet 320 
                                                      OpDecorate %320 Binding 320 
                                                      OpDecorate %321 RelaxedPrecision 
                                                      OpDecorate %324 RelaxedPrecision 
                                                      OpDecorate %324 DescriptorSet 324 
                                                      OpDecorate %324 Binding 324 
                                                      OpDecorate %325 RelaxedPrecision 
                                                      OpDecorate %330 RelaxedPrecision 
                                                      OpDecorate %331 RelaxedPrecision 
                                                      OpDecorate %332 RelaxedPrecision 
                                                      OpDecorate %334 RelaxedPrecision 
                                                      OpDecorate %334 Location 334 
                                                      OpDecorate %336 RelaxedPrecision 
                                                      OpDecorate %337 RelaxedPrecision 
                                                      OpDecorate %338 RelaxedPrecision 
                                                      OpDecorate %339 RelaxedPrecision 
                                                      OpDecorate %340 RelaxedPrecision 
                                                      OpDecorate %344 RelaxedPrecision 
                                               %2 = OpTypeVoid 
                                               %3 = OpTypeFunction %2 
                                               %6 = OpTypeFloat 32 
                                               %7 = OpTypeVector %6 4 
                                               %8 = OpTypePointer Private %7 
                                Private f32_4* %9 = OpVariable Private 
                                              %10 = OpTypePointer Input %7 
                                 Input f32_4* %11 = OpVariable Input 
                                              %13 = OpTypeInt 32 0 
                                          u32 %14 = OpConstant 128 
                                              %15 = OpTypeArray %6 %14 
                                              %16 = OpTypeStruct %7 %7 %7 %6 %6 %6 %15 %6 
                                              %17 = OpTypePointer Uniform %16 
Uniform struct {f32_4; f32_4; f32_4; f32; f32; f32; f32[128]; f32;}* %18 = OpVariable Uniform 
                                              %19 = OpTypeInt 32 1 
                                          i32 %20 = OpConstant 0 
                                              %21 = OpTypePointer Uniform %7 
                               Private f32_4* %25 = OpVariable Private 
                                          i32 %27 = OpConstant 1 
                               Private f32_4* %31 = OpVariable Private 
                                          i32 %33 = OpConstant 2 
                                              %37 = OpTypePointer Private %6 
                                 Private f32* %38 = OpVariable Private 
                                              %39 = OpTypeVector %6 2 
                                              %40 = OpTypePointer Input %39 
                        Input f32_2* vs_TEXCOORD0 = OpVariable Input 
                                          u32 %42 = OpConstant 0 
                                              %43 = OpTypePointer Input %6 
                                          i32 %46 = OpConstant 7 
                                              %47 = OpTypePointer Uniform %6 
                                              %53 = OpTypePointer Private %13 
                                 Private u32* %54 = OpVariable Private 
                                              %57 = OpTypeBool 
                                              %58 = OpTypeVector %57 2 
                                              %59 = OpTypePointer Private %58 
                              Private bool_2* %60 = OpVariable Private 
                                          i32 %61 = OpConstant 3 
                                          i32 %64 = OpConstant 4 
                                          i32 %72 = OpConstant 6 
                                              %78 = OpTypeVector %57 4 
                                          u32 %81 = OpConstant 1 
                                              %82 = OpTypePointer Private %57 
                                          f32 %97 = OpConstant 3.674022E-40 
                                Private f32* %100 = OpVariable Private 
                                         u32 %108 = OpConstant 3 
                                Private f32* %113 = OpVariable Private 
                             Private bool_2* %122 = OpVariable Private 
                                Private f32* %131 = OpVariable Private 
                                         f32 %134 = OpConstant 3.674022E-40 
                                         f32 %136 = OpConstant 3.674022E-40 
                                         f32 %140 = OpConstant 3.674022E-40 
                             Private bool_2* %142 = OpVariable Private 
                                             %149 = OpTypePointer Function %6 
                                         f32 %164 = OpConstant 3.674022E-40 
                                         i32 %170 = OpConstant 5 
                                         f32 %173 = OpConstant 3.674022E-40 
                                       f32_4 %194 = OpConstantComposite %136 %136 %136 %136 
                                       f32_2 %203 = OpConstantComposite %173 %173 
                                             %240 = OpTypePointer Function %7 
                                         f32 %262 = OpConstant 3.674022E-40 
                                       f32_2 %263 = OpConstantComposite %262 %136 
                                         f32 %269 = OpConstant 3.674022E-40 
                                         f32 %285 = OpConstant 3.674022E-40 
                                Private f32* %290 = OpVariable Private 
                              Private f32_4* %317 = OpVariable Private 
                                             %318 = OpTypeImage %6 Dim2D 0 0 0 1 Unknown 
                                             %319 = OpTypePointer UniformConstant %318 
        UniformConstant read_only Texture2D* %320 = OpVariable UniformConstant 
                                             %322 = OpTypeSampler 
                                             %323 = OpTypePointer UniformConstant %322 
                    UniformConstant sampler* %324 = OpVariable UniformConstant 
                                             %326 = OpTypeSampledImage %318 
                                             %333 = OpTypePointer Output %7 
                               Output f32_4* %334 = OpVariable Output 
                                             %335 = OpTypeVector %6 3 
                                             %345 = OpTypePointer Output %6 
                                          void %4 = OpFunction None %3 
                                               %5 = OpLabel 
                               Function f32* %150 = OpVariable Function 
                               Function f32* %161 = OpVariable Function 
                             Function f32_4* %241 = OpVariable Function 
                             Function f32_4* %251 = OpVariable Function 
                               Function f32* %293 = OpVariable Function 
                               Function f32* %308 = OpVariable Function 
                                        f32_4 %12 = OpLoad %11 
                               Uniform f32_4* %22 = OpAccessChain %18 %20 
                                        f32_4 %23 = OpLoad %22 
                                        f32_4 %24 = OpFMul %12 %23 
                                                      OpStore %9 %24 
                                        f32_4 %26 = OpLoad %11 
                               Uniform f32_4* %28 = OpAccessChain %18 %27 
                                        f32_4 %29 = OpLoad %28 
                                        f32_4 %30 = OpFMul %26 %29 
                                                      OpStore %25 %30 
                                        f32_4 %32 = OpLoad %11 
                               Uniform f32_4* %34 = OpAccessChain %18 %33 
                                        f32_4 %35 = OpLoad %34 
                                        f32_4 %36 = OpFMul %32 %35 
                                                      OpStore %31 %36 
                                   Input f32* %44 = OpAccessChain vs_TEXCOORD0 %42 
                                          f32 %45 = OpLoad %44 
                                 Uniform f32* %48 = OpAccessChain %18 %46 
                                          f32 %49 = OpLoad %48 
                                          f32 %50 = OpFMul %45 %49 
                                                      OpStore %38 %50 
                                          f32 %51 = OpLoad %38 
                                          f32 %52 = OpExtInst %1 8 %51 
                                                      OpStore %38 %52 
                                          f32 %55 = OpLoad %38 
                                          u32 %56 = OpConvertFToU %55 
                                                      OpStore %54 %56 
                                 Uniform f32* %62 = OpAccessChain %18 %61 
                                          f32 %63 = OpLoad %62 
                                 Uniform f32* %65 = OpAccessChain %18 %64 
                                          f32 %66 = OpLoad %65 
                                 Uniform f32* %67 = OpAccessChain %18 %61 
                                          f32 %68 = OpLoad %67 
                                 Uniform f32* %69 = OpAccessChain %18 %61 
                                          f32 %70 = OpLoad %69 
                                        f32_4 %71 = OpCompositeConstruct %63 %66 %68 %70 
                                          u32 %73 = OpLoad %54 
                                          i32 %74 = OpBitcast %73 
                                 Uniform f32* %75 = OpAccessChain %18 %72 %74 
                                          f32 %76 = OpLoad %75 
                                        f32_4 %77 = OpCompositeConstruct %76 %76 %76 %76 
                                       bool_4 %79 = OpFOrdLessThan %71 %77 
                                       bool_2 %80 = OpVectorShuffle %79 %79 0 1 
                                                      OpStore %60 %80 
                                Private bool* %83 = OpAccessChain %60 %81 
                                         bool %84 = OpLoad %83 
                                        f32_4 %85 = OpLoad %25 
                                        f32_4 %86 = OpLoad %31 
                                       bool_4 %87 = OpCompositeConstruct %84 %84 %84 %84 
                                        f32_4 %88 = OpSelect %87 %85 %86 
                                                      OpStore %25 %88 
                                Private bool* %89 = OpAccessChain %60 %42 
                                         bool %90 = OpLoad %89 
                                        f32_4 %91 = OpLoad %9 
                                        f32_4 %92 = OpLoad %25 
                                       bool_4 %93 = OpCompositeConstruct %90 %90 %90 %90 
                                        f32_4 %94 = OpSelect %93 %91 %92 
                                                      OpStore %9 %94 
                                   Input f32* %95 = OpAccessChain vs_TEXCOORD0 %81 
                                          f32 %96 = OpLoad %95 
                                          f32 %98 = OpFMul %96 %97 
                                 Private f32* %99 = OpAccessChain %25 %42 
                                                      OpStore %99 %98 
                                Private f32* %101 = OpAccessChain %25 %42 
                                         f32 %102 = OpLoad %101 
                                         u32 %103 = OpLoad %54 
                                         i32 %104 = OpBitcast %103 
                                Uniform f32* %105 = OpAccessChain %18 %72 %104 
                                         f32 %106 = OpLoad %105 
                                         f32 %107 = OpFDiv %102 %106 
                                                      OpStore %100 %107 
                                Private f32* %109 = OpAccessChain %9 %108 
                                         f32 %110 = OpLoad %109 
                                         f32 %111 = OpLoad %100 
                                         f32 %112 = OpFMul %110 %111 
                                                      OpStore %100 %112 
                                  Input f32* %114 = OpAccessChain vs_TEXCOORD0 %81 
                                         f32 %115 = OpLoad %114 
                                         f32 %116 = OpFNegate %115 
                                         u32 %117 = OpLoad %54 
                                         i32 %118 = OpBitcast %117 
                                Uniform f32* %119 = OpAccessChain %18 %72 %118 
                                         f32 %120 = OpLoad %119 
                                         f32 %121 = OpFAdd %116 %120 
                                                      OpStore %113 %121 
                                         u32 %123 = OpLoad %54 
                                         i32 %124 = OpBitcast %123 
                                Uniform f32* %125 = OpAccessChain %18 %72 %124 
                                         f32 %126 = OpLoad %125 
                                  Input f32* %127 = OpAccessChain vs_TEXCOORD0 %81 
                                         f32 %128 = OpLoad %127 
                                        bool %129 = OpFOrdLessThan %126 %128 
                               Private bool* %130 = OpAccessChain %122 %42 
                                                      OpStore %130 %129 
                                Uniform f32* %132 = OpAccessChain %18 %46 
                                         f32 %133 = OpLoad %132 
                                         f32 %135 = OpFAdd %133 %134 
                                                      OpStore %131 %135 
                                         f32 %137 = OpLoad %131 
                                         f32 %138 = OpFDiv %136 %137 
                                                      OpStore %131 %138 
                                         f32 %139 = OpLoad %131 
                                         f32 %141 = OpFMul %139 %140 
                                                      OpStore %131 %141 
                                         f32 %143 = OpLoad %131 
                                         f32 %144 = OpLoad %113 
                                        bool %145 = OpFOrdLessThan %143 %144 
                               Private bool* %146 = OpAccessChain %142 %42 
                                                      OpStore %146 %145 
                               Private bool* %147 = OpAccessChain %142 %42 
                                        bool %148 = OpLoad %147 
                                                      OpSelectionMerge %152 None 
                                                      OpBranchConditional %148 %151 %154 
                                             %151 = OpLabel 
                                         f32 %153 = OpLoad %100 
                                                      OpStore %150 %153 
                                                      OpBranch %152 
                                             %154 = OpLabel 
                                Private f32* %155 = OpAccessChain %9 %108 
                                         f32 %156 = OpLoad %155 
                                                      OpStore %150 %156 
                                                      OpBranch %152 
                                             %152 = OpLabel 
                                         f32 %157 = OpLoad %150 
                                Private f32* %158 = OpAccessChain %25 %42 
                                                      OpStore %158 %157 
                               Private bool* %159 = OpAccessChain %122 %42 
                                        bool %160 = OpLoad %159 
                                                      OpSelectionMerge %163 None 
                                                      OpBranchConditional %160 %162 %165 
                                             %162 = OpLabel 
                                                      OpStore %161 %164 
                                                      OpBranch %163 
                                             %165 = OpLabel 
                                Private f32* %166 = OpAccessChain %25 %42 
                                         f32 %167 = OpLoad %166 
                                                      OpStore %161 %167 
                                                      OpBranch %163 
                                             %163 = OpLabel 
                                         f32 %168 = OpLoad %161 
                                Private f32* %169 = OpAccessChain %9 %108 
                                                      OpStore %169 %168 
                                Uniform f32* %171 = OpAccessChain %18 %170 
                                         f32 %172 = OpLoad %171 
                                         f32 %174 = OpFAdd %172 %173 
                                                      OpStore %38 %174 
                                         f32 %175 = OpLoad %38 
                                  Input f32* %176 = OpAccessChain vs_TEXCOORD0 %81 
                                         f32 %177 = OpLoad %176 
                                        bool %178 = OpFOrdLessThan %175 %177 
                               Private bool* %179 = OpAccessChain %122 %42 
                                                      OpStore %179 %178 
                                  Input f32* %180 = OpAccessChain vs_TEXCOORD0 %81 
                                         f32 %181 = OpLoad %180 
                                Uniform f32* %182 = OpAccessChain %18 %170 
                                         f32 %183 = OpLoad %182 
                                        bool %184 = OpFOrdLessThan %181 %183 
                               Private bool* %185 = OpAccessChain %60 %42 
                                                      OpStore %185 %184 
                               Private bool* %186 = OpAccessChain %122 %42 
                                        bool %187 = OpLoad %186 
                               Private bool* %188 = OpAccessChain %60 %42 
                                        bool %189 = OpLoad %188 
                                        bool %190 = OpLogicalAnd %187 %189 
                               Private bool* %191 = OpAccessChain %122 %42 
                                                      OpStore %191 %190 
                               Private bool* %192 = OpAccessChain %122 %42 
                                        bool %193 = OpLoad %192 
                                       f32_4 %195 = OpLoad %9 
                                      bool_4 %196 = OpCompositeConstruct %193 %193 %193 %193 
                                       f32_4 %197 = OpSelect %196 %194 %195 
                                                      OpStore %9 %197 
                                Uniform f32* %198 = OpAccessChain %18 %64 
                                         f32 %199 = OpLoad %198 
                                Uniform f32* %200 = OpAccessChain %18 %61 
                                         f32 %201 = OpLoad %200 
                                       f32_2 %202 = OpCompositeConstruct %199 %201 
                                       f32_2 %204 = OpFAdd %202 %203 
                                       f32_4 %205 = OpLoad %25 
                                       f32_4 %206 = OpVectorShuffle %205 %204 4 5 2 3 
                                                      OpStore %25 %206 
                                       f32_4 %207 = OpLoad %25 
                                       f32_4 %208 = OpVectorShuffle %207 %207 0 1 0 0 
                                       f32_2 %209 = OpLoad vs_TEXCOORD0 
                                       f32_4 %210 = OpVectorShuffle %209 %209 1 1 1 1 
                                      bool_4 %211 = OpFOrdLessThan %208 %210 
                                      bool_2 %212 = OpVectorShuffle %211 %211 0 1 
                                                      OpStore %122 %212 
                                       f32_2 %213 = OpLoad vs_TEXCOORD0 
                                       f32_4 %214 = OpVectorShuffle %213 %213 1 1 1 1 
                                Uniform f32* %215 = OpAccessChain %18 %64 
                                         f32 %216 = OpLoad %215 
                                Uniform f32* %217 = OpAccessChain %18 %61 
                                         f32 %218 = OpLoad %217 
                                Uniform f32* %219 = OpAccessChain %18 %64 
                                         f32 %220 = OpLoad %219 
                                Uniform f32* %221 = OpAccessChain %18 %61 
                                         f32 %222 = OpLoad %221 
                                       f32_4 %223 = OpCompositeConstruct %216 %218 %220 %222 
                                      bool_4 %224 = OpFOrdLessThan %214 %223 
                                      bool_2 %225 = OpVectorShuffle %224 %224 0 1 
                                                      OpStore %142 %225 
                               Private bool* %226 = OpAccessChain %122 %42 
                                        bool %227 = OpLoad %226 
                               Private bool* %228 = OpAccessChain %142 %42 
                                        bool %229 = OpLoad %228 
                                        bool %230 = OpLogicalAnd %227 %229 
                               Private bool* %231 = OpAccessChain %122 %42 
                                                      OpStore %231 %230 
                               Private bool* %232 = OpAccessChain %122 %81 
                                        bool %233 = OpLoad %232 
                               Private bool* %234 = OpAccessChain %142 %81 
                                        bool %235 = OpLoad %234 
                                        bool %236 = OpLogicalAnd %233 %235 
                               Private bool* %237 = OpAccessChain %122 %81 
                                                      OpStore %237 %236 
                               Private bool* %238 = OpAccessChain %122 %42 
                                        bool %239 = OpLoad %238 
                                                      OpSelectionMerge %243 None 
                                                      OpBranchConditional %239 %242 %246 
                                             %242 = OpLabel 
                              Uniform f32_4* %244 = OpAccessChain %18 %27 
                                       f32_4 %245 = OpLoad %244 
                                                      OpStore %241 %245 
                                                      OpBranch %243 
                                             %246 = OpLabel 
                                       f32_4 %247 = OpLoad %9 
                                                      OpStore %241 %247 
                                                      OpBranch %243 
                                             %243 = OpLabel 
                                       f32_4 %248 = OpLoad %241 
                                                      OpStore %9 %248 
                               Private bool* %249 = OpAccessChain %122 %81 
                                        bool %250 = OpLoad %249 
                                                      OpSelectionMerge %253 None 
                                                      OpBranchConditional %250 %252 %256 
                                             %252 = OpLabel 
                              Uniform f32_4* %254 = OpAccessChain %18 %20 
                                       f32_4 %255 = OpLoad %254 
                                                      OpStore %251 %255 
                                                      OpBranch %253 
                                             %256 = OpLabel 
                                       f32_4 %257 = OpLoad %9 
                                                      OpStore %251 %257 
                                                      OpBranch %253 
                                             %253 = OpLabel 
                                       f32_4 %258 = OpLoad %251 
                                                      OpStore %9 %258 
                                       f32_2 %259 = OpLoad vs_TEXCOORD0 
                                       f32_2 %260 = OpVectorShuffle %259 %259 0 0 
                                       f32_2 %261 = OpFNegate %260 
                                       f32_2 %264 = OpFAdd %261 %263 
                                       f32_4 %265 = OpLoad %25 
                                       f32_4 %266 = OpVectorShuffle %265 %264 4 5 2 3 
                                                      OpStore %25 %266 
                                Private f32* %267 = OpAccessChain %25 %81 
                                         f32 %268 = OpLoad %267 
                                         f32 %270 = OpFMul %268 %269 
                                Private f32* %271 = OpAccessChain %25 %81 
                                                      OpStore %271 %270 
                                Private f32* %272 = OpAccessChain %25 %42 
                                         f32 %273 = OpLoad %272 
                                         f32 %274 = OpFNegate %273 
                                         f32 %275 = OpFMul %274 %269 
                                         f32 %276 = OpFAdd %275 %136 
                                Private f32* %277 = OpAccessChain %25 %42 
                                                      OpStore %277 %276 
                                       f32_4 %278 = OpLoad %9 
                                       f32_2 %279 = OpVectorShuffle %278 %278 3 3 
                                       f32_4 %280 = OpLoad %25 
                                       f32_2 %281 = OpVectorShuffle %280 %280 0 1 
                                       f32_2 %282 = OpFMul %279 %281 
                                       f32_4 %283 = OpLoad %25 
                                       f32_4 %284 = OpVectorShuffle %283 %282 4 5 2 3 
                                                      OpStore %25 %284 
                                  Input f32* %286 = OpAccessChain vs_TEXCOORD0 %42 
                                         f32 %287 = OpLoad %286 
                                        bool %288 = OpFOrdLessThan %285 %287 
                               Private bool* %289 = OpAccessChain %122 %42 
                                                      OpStore %289 %288 
                               Private bool* %291 = OpAccessChain %122 %42 
                                        bool %292 = OpLoad %291 
                                                      OpSelectionMerge %295 None 
                                                      OpBranchConditional %292 %294 %298 
                                             %294 = OpLabel 
                                Private f32* %296 = OpAccessChain %25 %81 
                                         f32 %297 = OpLoad %296 
                                                      OpStore %293 %297 
                                                      OpBranch %295 
                                             %298 = OpLabel 
                                Private f32* %299 = OpAccessChain %9 %108 
                                         f32 %300 = OpLoad %299 
                                                      OpStore %293 %300 
                                                      OpBranch %295 
                                             %295 = OpLabel 
                                         f32 %301 = OpLoad %293 
                                                      OpStore %290 %301 
                                  Input f32* %302 = OpAccessChain vs_TEXCOORD0 %42 
                                         f32 %303 = OpLoad %302 
                                        bool %304 = OpFOrdLessThan %303 %262 
                               Private bool* %305 = OpAccessChain %122 %42 
                                                      OpStore %305 %304 
                               Private bool* %306 = OpAccessChain %122 %42 
                                        bool %307 = OpLoad %306 
                                                      OpSelectionMerge %310 None 
                                                      OpBranchConditional %307 %309 %313 
                                             %309 = OpLabel 
                                Private f32* %311 = OpAccessChain %25 %42 
                                         f32 %312 = OpLoad %311 
                                                      OpStore %308 %312 
                                                      OpBranch %310 
                                             %313 = OpLabel 
                                         f32 %314 = OpLoad %290 
                                                      OpStore %308 %314 
                                                      OpBranch %310 
                                             %310 = OpLabel 
                                         f32 %315 = OpLoad %308 
                                Private f32* %316 = OpAccessChain %9 %108 
                                                      OpStore %316 %315 
                         read_only Texture2D %321 = OpLoad %320 
                                     sampler %325 = OpLoad %324 
                  read_only Texture2DSampled %327 = OpSampledImage %321 %325 
                                       f32_2 %328 = OpLoad vs_TEXCOORD0 
                                       f32_4 %329 = OpImageSampleImplicitLod %327 %328 
                                                      OpStore %317 %329 
                                       f32_4 %330 = OpLoad %9 
                                       f32_4 %331 = OpLoad %317 
                                       f32_4 %332 = OpFMul %330 %331 
                                                      OpStore %9 %332 
                                       f32_4 %336 = OpLoad %9 
                                       f32_3 %337 = OpVectorShuffle %336 %336 3 3 3 
                                       f32_4 %338 = OpLoad %9 
                                       f32_3 %339 = OpVectorShuffle %338 %338 0 1 2 
                                       f32_3 %340 = OpFMul %337 %339 
                                       f32_4 %341 = OpLoad %334 
                                       f32_4 %342 = OpVectorShuffle %341 %340 4 5 6 3 
                                                      OpStore %334 %342 
                                Private f32* %343 = OpAccessChain %9 %108 
                                         f32 %344 = OpLoad %343 
                                 Output f32* %346 = OpAccessChain %334 %108 
                                                      OpStore %346 %344 
                                                      OpReturn
                                                      OpFunctionEnd
"
}
SubProgram "gles3 " {
Keywords { "PIXELSNAP_ON" }
"#ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ScreenParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	mediump vec4 _Color;
in highp vec4 in_POSITION0;
in highp vec4 in_COLOR0;
in highp vec2 in_TEXCOORD0;
out mediump vec4 vs_COLOR0;
out highp vec2 vs_TEXCOORD0;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    u_xlat0.xy = u_xlat0.xy / u_xlat0.ww;
    u_xlat1.xy = _ScreenParams.xy * vec2(0.5, 0.5);
    u_xlat0.xy = u_xlat0.xy * u_xlat1.xy;
    u_xlat0.xy = roundEven(u_xlat0.xy);
    u_xlat0.xy = u_xlat0.xy / u_xlat1.xy;
    gl_Position.xy = u_xlat0.ww * u_xlat0.xy;
    gl_Position.zw = u_xlat0.zw;
    u_xlat0 = in_COLOR0 * _Color;
    vs_COLOR0 = u_xlat0;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	mediump vec4 _GoodColor;
uniform 	mediump vec4 _CautionColor;
uniform 	mediump vec4 _CriticalColor;
uniform 	mediump float _GoodThreshold;
uniform 	mediump float _CautionThreshold;
uniform 	float Average;
uniform 	float GraphValues[128];
uniform 	float GraphValues_Length;
UNITY_LOCATION(0) uniform mediump sampler2D _MainTex;
in mediump vec4 vs_COLOR0;
in highp vec2 vs_TEXCOORD0;
layout(location = 0) out mediump vec4 SV_Target0;
mediump vec4 u_xlat16_0;
mediump vec4 u_xlat16_1;
mediump vec4 u_xlat16_2;
float u_xlat3;
uint u_xlatu3;
bvec2 u_xlatb3;
mediump float u_xlat16_5;
float u_xlat7;
bvec2 u_xlatb7;
float u_xlat11;
bvec2 u_xlatb11;
float u_xlat15;
void main()
{
    u_xlat16_0 = vs_COLOR0 * _GoodColor;
    u_xlat16_1 = vs_COLOR0 * _CautionColor;
    u_xlat16_2 = vs_COLOR0 * _CriticalColor;
    u_xlat3 = vs_TEXCOORD0.x * GraphValues_Length;
    u_xlat3 = floor(u_xlat3);
    u_xlatu3 = uint(u_xlat3);
    u_xlatb7.xy = lessThan(vec4(_GoodThreshold, _CautionThreshold, _GoodThreshold, _GoodThreshold), vec4(GraphValues[int(u_xlatu3)])).xy;
    u_xlat16_1 = (u_xlatb7.y) ? u_xlat16_1 : u_xlat16_2;
    u_xlat16_0 = (u_xlatb7.x) ? u_xlat16_0 : u_xlat16_1;
    u_xlat16_1.x = vs_TEXCOORD0.y * 0.300000012;
    u_xlat7 = u_xlat16_1.x / GraphValues[int(u_xlatu3)];
    u_xlat7 = u_xlat16_0.w * u_xlat7;
    u_xlat11 = (-vs_TEXCOORD0.y) + GraphValues[int(u_xlatu3)];
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(GraphValues[int(u_xlatu3)]<vs_TEXCOORD0.y);
#else
    u_xlatb3.x = GraphValues[int(u_xlatu3)]<vs_TEXCOORD0.y;
#endif
    u_xlat15 = GraphValues_Length + -1.0;
    u_xlat15 = float(1.0) / u_xlat15;
    u_xlat15 = u_xlat15 * 4.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb11.x = !!(u_xlat15<u_xlat11);
#else
    u_xlatb11.x = u_xlat15<u_xlat11;
#endif
    u_xlat16_1.x = (u_xlatb11.x) ? u_xlat7 : u_xlat16_0.w;
    u_xlat16_0.w = (u_xlatb3.x) ? 0.0 : u_xlat16_1.x;
    u_xlat3 = Average + -0.0199999996;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(u_xlat3<vs_TEXCOORD0.y);
#else
    u_xlatb3.x = u_xlat3<vs_TEXCOORD0.y;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb7.x = !!(vs_TEXCOORD0.y<Average);
#else
    u_xlatb7.x = vs_TEXCOORD0.y<Average;
#endif
    u_xlatb3.x = u_xlatb3.x && u_xlatb7.x;
    u_xlat16_0 = (u_xlatb3.x) ? vec4(1.0, 1.0, 1.0, 1.0) : u_xlat16_0;
    u_xlat16_1.xy = vec2(_CautionThreshold, _GoodThreshold) + vec2(-0.0199999996, -0.0199999996);
    u_xlatb3.xy = lessThan(u_xlat16_1.xyxx, vs_TEXCOORD0.yyyy).xy;
    u_xlatb11.xy = lessThan(vs_TEXCOORD0.yyyy, vec4(_CautionThreshold, _GoodThreshold, _CautionThreshold, _GoodThreshold)).xy;
    u_xlatb3.x = u_xlatb3.x && u_xlatb11.x;
    u_xlatb3.y = u_xlatb3.y && u_xlatb11.y;
    u_xlat16_0 = (u_xlatb3.x) ? _CautionColor : u_xlat16_0;
    u_xlat16_0 = (u_xlatb3.y) ? _GoodColor : u_xlat16_0;
    u_xlat16_1.xy = (-vs_TEXCOORD0.xx) + vec2(0.0299999993, 1.0);
    u_xlat16_1.y = u_xlat16_1.y * 33.3333359;
    u_xlat16_1.x = (-u_xlat16_1.x) * 33.3333359 + 1.0;
    u_xlat16_1.xy = u_xlat16_0.ww * u_xlat16_1.xy;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(0.970000029<vs_TEXCOORD0.x);
#else
    u_xlatb3.x = 0.970000029<vs_TEXCOORD0.x;
#endif
    u_xlat16_5 = (u_xlatb3.x) ? u_xlat16_1.y : u_xlat16_0.w;
#ifdef UNITY_ADRENO_ES3
    u_xlatb3.x = !!(vs_TEXCOORD0.x<0.0299999993);
#else
    u_xlatb3.x = vs_TEXCOORD0.x<0.0299999993;
#endif
    u_xlat16_0.w = (u_xlatb3.x) ? u_xlat16_1.x : u_xlat16_5;
    u_xlat16_1 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat16_0 = u_xlat16_0 * u_xlat16_1;
    SV_Target0.xyz = u_xlat16_0.www * u_xlat16_0.xyz;
    SV_Target0.w = u_xlat16_0.w;
    return;
}

#endif
"
}
SubProgram "vulkan " {
Keywords { "PIXELSNAP_ON" }
"; SPIR-V
; Version: 1.0
; Generator: Khronos Glslang Reference Front End; 6
; Bound: 147
; Schema: 0
                                                      OpCapability Shader 
                                               %1 = OpExtInstImport "GLSL.std.450" 
                                                      OpMemoryModel Logical GLSL450 
                                                      OpEntryPoint Vertex %4 "main" %11 %114 %129 %134 %137 %139 
                                                      OpName vs_TEXCOORD0 "vs_TEXCOORD0" 
                                                      OpDecorate %11 Location 11 
                                                      OpDecorate %16 ArrayStride 16 
                                                      OpDecorate %17 ArrayStride 17 
                                                      OpMemberDecorate %18 0 Offset 18 
                                                      OpMemberDecorate %18 1 Offset 18 
                                                      OpMemberDecorate %18 2 Offset 18 
                                                      OpMemberDecorate %18 3 RelaxedPrecision 
                                                      OpMemberDecorate %18 3 Offset 18 
                                                      OpDecorate %18 Block 
                                                      OpDecorate %20 DescriptorSet 20 
                                                      OpDecorate %20 Binding 20 
                                                      OpMemberDecorate %112 0 BuiltIn 112 
                                                      OpMemberDecorate %112 1 BuiltIn 112 
                                                      OpMemberDecorate %112 2 BuiltIn 112 
                                                      OpDecorate %112 Block 
                                                      OpDecorate %129 Location 129 
                                                      OpDecorate %132 RelaxedPrecision 
                                                      OpDecorate %134 RelaxedPrecision 
                                                      OpDecorate %134 Location 134 
                                                      OpDecorate vs_TEXCOORD0 Location 137 
                                                      OpDecorate %139 Location 139 
                                               %2 = OpTypeVoid 
                                               %3 = OpTypeFunction %2 
                                               %6 = OpTypeFloat 32 
                                               %7 = OpTypeVector %6 4 
                                               %8 = OpTypePointer Private %7 
                                Private f32_4* %9 = OpVariable Private 
                                              %10 = OpTypePointer Input %7 
                                 Input f32_4* %11 = OpVariable Input 
                                              %14 = OpTypeInt 32 0 
                                          u32 %15 = OpConstant 4 
                                              %16 = OpTypeArray %7 %15 
                                              %17 = OpTypeArray %7 %15 
                                              %18 = OpTypeStruct %7 %16 %17 %7 
                                              %19 = OpTypePointer Uniform %18 
Uniform struct {f32_4; f32_4[4]; f32_4[4]; f32_4;}* %20 = OpVariable Uniform 
                                              %21 = OpTypeInt 32 1 
                                          i32 %22 = OpConstant 1 
                                              %23 = OpTypePointer Uniform %7 
                                          i32 %27 = OpConstant 0 
                                          i32 %35 = OpConstant 2 
                                          i32 %44 = OpConstant 3 
                               Private f32_4* %48 = OpVariable Private 
                                              %75 = OpTypeVector %6 2 
                                          f32 %86 = OpConstant 3.674022E-40 
                                        f32_2 %87 = OpConstantComposite %86 %86 
                                         u32 %110 = OpConstant 1 
                                             %111 = OpTypeArray %6 %110 
                                             %112 = OpTypeStruct %7 %6 %111 
                                             %113 = OpTypePointer Output %112 
        Output struct {f32_4; f32; f32[1];}* %114 = OpVariable Output 
                                             %120 = OpTypePointer Output %7 
                                Input f32_4* %129 = OpVariable Input 
                               Output f32_4* %134 = OpVariable Output 
                                             %136 = OpTypePointer Output %75 
                       Output f32_2* vs_TEXCOORD0 = OpVariable Output 
                                             %138 = OpTypePointer Input %75 
                                Input f32_2* %139 = OpVariable Input 
                                             %141 = OpTypePointer Output %6 
                                          void %4 = OpFunction None %3 
                                               %5 = OpLabel 
                                        f32_4 %12 = OpLoad %11 
                                        f32_4 %13 = OpVectorShuffle %12 %12 1 1 1 1 
                               Uniform f32_4* %24 = OpAccessChain %20 %22 %22 
                                        f32_4 %25 = OpLoad %24 
                                        f32_4 %26 = OpFMul %13 %25 
                                                      OpStore %9 %26 
                               Uniform f32_4* %28 = OpAccessChain %20 %22 %27 
                                        f32_4 %29 = OpLoad %28 
                                        f32_4 %30 = OpLoad %11 
                                        f32_4 %31 = OpVectorShuffle %30 %30 0 0 0 0 
                                        f32_4 %32 = OpFMul %29 %31 
                                        f32_4 %33 = OpLoad %9 
                                        f32_4 %34 = OpFAdd %32 %33 
                                                      OpStore %9 %34 
                               Uniform f32_4* %36 = OpAccessChain %20 %22 %35 
                                        f32_4 %37 = OpLoad %36 
                                        f32_4 %38 = OpLoad %11 
                                        f32_4 %39 = OpVectorShuffle %38 %38 2 2 2 2 
                                        f32_4 %40 = OpFMul %37 %39 
                                        f32_4 %41 = OpLoad %9 
                                        f32_4 %42 = OpFAdd %40 %41 
                                                      OpStore %9 %42 
                                        f32_4 %43 = OpLoad %9 
                               Uniform f32_4* %45 = OpAccessChain %20 %22 %44 
                                        f32_4 %46 = OpLoad %45 
                                        f32_4 %47 = OpFAdd %43 %46 
                                                      OpStore %9 %47 
                                        f32_4 %49 = OpLoad %9 
                                        f32_4 %50 = OpVectorShuffle %49 %49 1 1 1 1 
                               Uniform f32_4* %51 = OpAccessChain %20 %35 %22 
                                        f32_4 %52 = OpLoad %51 
                                        f32_4 %53 = OpFMul %50 %52 
                                                      OpStore %48 %53 
                               Uniform f32_4* %54 = OpAccessChain %20 %35 %27 
                                        f32_4 %55 = OpLoad %54 
                                        f32_4 %56 = OpLoad %9 
                                        f32_4 %57 = OpVectorShuffle %56 %56 0 0 0 0 
                                        f32_4 %58 = OpFMul %55 %57 
                                        f32_4 %59 = OpLoad %48 
                                        f32_4 %60 = OpFAdd %58 %59 
                                                      OpStore %48 %60 
                               Uniform f32_4* %61 = OpAccessChain %20 %35 %35 
                                        f32_4 %62 = OpLoad %61 
                                        f32_4 %63 = OpLoad %9 
                                        f32_4 %64 = OpVectorShuffle %63 %63 2 2 2 2 
                                        f32_4 %65 = OpFMul %62 %64 
                                        f32_4 %66 = OpLoad %48 
                                        f32_4 %67 = OpFAdd %65 %66 
                                                      OpStore %48 %67 
                               Uniform f32_4* %68 = OpAccessChain %20 %35 %44 
                                        f32_4 %69 = OpLoad %68 
                                        f32_4 %70 = OpLoad %9 
                                        f32_4 %71 = OpVectorShuffle %70 %70 3 3 3 3 
                                        f32_4 %72 = OpFMul %69 %71 
                                        f32_4 %73 = OpLoad %48 
                                        f32_4 %74 = OpFAdd %72 %73 
                                                      OpStore %9 %74 
                                        f32_4 %76 = OpLoad %9 
                                        f32_2 %77 = OpVectorShuffle %76 %76 0 1 
                                        f32_4 %78 = OpLoad %9 
                                        f32_2 %79 = OpVectorShuffle %78 %78 3 3 
                                        f32_2 %80 = OpFDiv %77 %79 
                                        f32_4 %81 = OpLoad %9 
                                        f32_4 %82 = OpVectorShuffle %81 %80 4 5 2 3 
                                                      OpStore %9 %82 
                               Uniform f32_4* %83 = OpAccessChain %20 %27 
                                        f32_4 %84 = OpLoad %83 
                                        f32_2 %85 = OpVectorShuffle %84 %84 0 1 
                                        f32_2 %88 = OpFMul %85 %87 
                                        f32_4 %89 = OpLoad %48 
                                        f32_4 %90 = OpVectorShuffle %89 %88 4 5 2 3 
                                                      OpStore %48 %90 
                                        f32_4 %91 = OpLoad %9 
                                        f32_2 %92 = OpVectorShuffle %91 %91 0 1 
                                        f32_4 %93 = OpLoad %48 
                                        f32_2 %94 = OpVectorShuffle %93 %93 0 1 
                                        f32_2 %95 = OpFMul %92 %94 
                                        f32_4 %96 = OpLoad %9 
                                        f32_4 %97 = OpVectorShuffle %96 %95 4 5 2 3 
                                                      OpStore %9 %97 
                                        f32_4 %98 = OpLoad %9 
                                        f32_2 %99 = OpVectorShuffle %98 %98 0 1 
                                       f32_2 %100 = OpExtInst %1 2 %99 
                                       f32_4 %101 = OpLoad %9 
                                       f32_4 %102 = OpVectorShuffle %101 %100 4 5 2 3 
                                                      OpStore %9 %102 
                                       f32_4 %103 = OpLoad %9 
                                       f32_2 %104 = OpVectorShuffle %103 %103 0 1 
                                       f32_4 %105 = OpLoad %48 
                                       f32_2 %106 = OpVectorShuffle %105 %105 0 1 
                                       f32_2 %107 = OpFDiv %104 %106 
                                       f32_4 %108 = OpLoad %9 
                                       f32_4 %109 = OpVectorShuffle %108 %107 4 5 2 3 
                                                      OpStore %9 %109 
                                       f32_4 %115 = OpLoad %9 
                                       f32_2 %116 = OpVectorShuffle %115 %115 3 3 
                                       f32_4 %117 = OpLoad %9 
                                       f32_2 %118 = OpVectorShuffle %117 %117 0 1 
                                       f32_2 %119 = OpFMul %116 %118 
                               Output f32_4* %121 = OpAccessChain %114 %27 
                                       f32_4 %122 = OpLoad %121 
                                       f32_4 %123 = OpVectorShuffle %122 %119 4 5 2 3 
                                                      OpStore %121 %123 
                                       f32_4 %124 = OpLoad %9 
                                       f32_2 %125 = OpVectorShuffle %124 %124 2 3 
                               Output f32_4* %126 = OpAccessChain %114 %27 
                                       f32_4 %127 = OpLoad %126 
                                       f32_4 %128 = OpVectorShuffle %127 %125 0 1 4 5 
                                                      OpStore %126 %128 
                                       f32_4 %130 = OpLoad %129 
                              Uniform f32_4* %131 = OpAccessChain %20 %44 
                                       f32_4 %132 = OpLoad %131 
                                       f32_4 %133 = OpFMul %130 %132 
                                                      OpStore %9 %133 
                                       f32_4 %135 = OpLoad %9 
                                                      OpStore %134 %135 
                                       f32_2 %140 = OpLoad %139 
                                                      OpStore vs_TEXCOORD0 %140 
                                 Output f32* %142 = OpAccessChain %114 %27 %110 
                                         f32 %143 = OpLoad %142 
                                         f32 %144 = OpFNegate %143 
                                 Output f32* %145 = OpAccessChain %114 %27 %110 
                                                      OpStore %145 %144 
                                                      OpReturn
                                                      OpFunctionEnd
; SPIR-V
; Version: 1.0
; Generator: Khronos Glslang Reference Front End; 6
; Bound: 348
; Schema: 0
                                                      OpCapability Shader 
                                               %1 = OpExtInstImport "GLSL.std.450" 
                                                      OpMemoryModel Logical GLSL450 
                                                      OpEntryPoint Fragment %4 "main" %11 %41 %334 
                                                      OpExecutionMode %4 OriginUpperLeft 
                                                      OpName vs_TEXCOORD0 "vs_TEXCOORD0" 
                                                      OpDecorate %9 RelaxedPrecision 
                                                      OpDecorate %11 RelaxedPrecision 
                                                      OpDecorate %11 Location 11 
                                                      OpDecorate %12 RelaxedPrecision 
                                                      OpDecorate %15 ArrayStride 15 
                                                      OpMemberDecorate %16 0 RelaxedPrecision 
                                                      OpMemberDecorate %16 0 Offset 16 
                                                      OpMemberDecorate %16 1 RelaxedPrecision 
                                                      OpMemberDecorate %16 1 Offset 16 
                                                      OpMemberDecorate %16 2 RelaxedPrecision 
                                                      OpMemberDecorate %16 2 Offset 16 
                                                      OpMemberDecorate %16 3 RelaxedPrecision 
                                                      OpMemberDecorate %16 3 Offset 16 
                                                      OpMemberDecorate %16 4 RelaxedPrecision 
                                                      OpMemberDecorate %16 4 Offset 16 
                                                      OpMemberDecorate %16 5 Offset 16 
                                                      OpMemberDecorate %16 6 Offset 16 
                                                      OpMemberDecorate %16 7 Offset 16 
                                                      OpDecorate %16 Block 
                                                      OpDecorate %18 DescriptorSet 18 
                                                      OpDecorate %18 Binding 18 
                                                      OpDecorate %23 RelaxedPrecision 
                                                      OpDecorate %24 RelaxedPrecision 
                                                      OpDecorate %25 RelaxedPrecision 
                                                      OpDecorate %26 RelaxedPrecision 
                                                      OpDecorate %29 RelaxedPrecision 
                                                      OpDecorate %30 RelaxedPrecision 
                                                      OpDecorate %31 RelaxedPrecision 
                                                      OpDecorate %32 RelaxedPrecision 
                                                      OpDecorate %35 RelaxedPrecision 
                                                      OpDecorate %36 RelaxedPrecision 
                                                      OpDecorate vs_TEXCOORD0 Location 41 
                                                      OpDecorate %63 RelaxedPrecision 
                                                      OpDecorate %66 RelaxedPrecision 
                                                      OpDecorate %68 RelaxedPrecision 
                                                      OpDecorate %70 RelaxedPrecision 
                                                      OpDecorate %85 RelaxedPrecision 
                                                      OpDecorate %86 RelaxedPrecision 
                                                      OpDecorate %91 RelaxedPrecision 
                                                      OpDecorate %92 RelaxedPrecision 
                                                      OpDecorate %102 RelaxedPrecision 
                                                      OpDecorate %110 RelaxedPrecision 
                                                      OpDecorate %156 RelaxedPrecision 
                                                      OpDecorate %167 RelaxedPrecision 
                                                      OpDecorate %168 RelaxedPrecision 
                                                      OpDecorate %199 RelaxedPrecision 
                                                      OpDecorate %201 RelaxedPrecision 
                                                      OpDecorate %202 RelaxedPrecision 
                                                      OpDecorate %204 RelaxedPrecision 
                                                      OpDecorate %207 RelaxedPrecision 
                                                      OpDecorate %208 RelaxedPrecision 
                                                      OpDecorate %216 RelaxedPrecision 
                                                      OpDecorate %218 RelaxedPrecision 
                                                      OpDecorate %220 RelaxedPrecision 
                                                      OpDecorate %222 RelaxedPrecision 
                                                      OpDecorate %245 RelaxedPrecision 
                                                      OpDecorate %247 RelaxedPrecision 
                                                      OpDecorate %248 RelaxedPrecision 
                                                      OpDecorate %255 RelaxedPrecision 
                                                      OpDecorate %257 RelaxedPrecision 
                                                      OpDecorate %258 RelaxedPrecision 
                                                      OpDecorate %268 RelaxedPrecision 
                                                      OpDecorate %270 RelaxedPrecision 
                                                      OpDecorate %273 RelaxedPrecision 
                                                      OpDecorate %274 RelaxedPrecision 
                                                      OpDecorate %275 RelaxedPrecision 
                                                      OpDecorate %276 RelaxedPrecision 
                                                      OpDecorate %278 RelaxedPrecision 
                                                      OpDecorate %279 RelaxedPrecision 
                                                      OpDecorate %280 RelaxedPrecision 
                                                      OpDecorate %281 RelaxedPrecision 
                                                      OpDecorate %282 RelaxedPrecision 
                                                      OpDecorate %290 RelaxedPrecision 
                                                      OpDecorate %297 RelaxedPrecision 
                                                      OpDecorate %300 RelaxedPrecision 
                                                      OpDecorate %301 RelaxedPrecision 
                                                      OpDecorate %312 RelaxedPrecision 
                                                      OpDecorate %314 RelaxedPrecision 
                                                      OpDecorate %315 RelaxedPrecision 
                                                      OpDecorate %317 RelaxedPrecision 
                                                      OpDecorate %320 RelaxedPrecision 
                                                      OpDecorate %320 DescriptorSet 320 
                                                      OpDecorate %320 Binding 320 
                                                      OpDecorate %321 RelaxedPrecision 
                                                      OpDecorate %324 RelaxedPrecision 
                                                      OpDecorate %324 DescriptorSet 324 
                                                      OpDecorate %324 Binding 324 
                                                      OpDecorate %325 RelaxedPrecision 
                                                      OpDecorate %330 RelaxedPrecision 
                                                      OpDecorate %331 RelaxedPrecision 
                                                      OpDecorate %332 RelaxedPrecision 
                                                      OpDecorate %334 RelaxedPrecision 
                                                      OpDecorate %334 Location 334 
                                                      OpDecorate %336 RelaxedPrecision 
                                                      OpDecorate %337 RelaxedPrecision 
                                                      OpDecorate %338 RelaxedPrecision 
                                                      OpDecorate %339 RelaxedPrecision 
                                                      OpDecorate %340 RelaxedPrecision 
                                                      OpDecorate %344 RelaxedPrecision 
                                               %2 = OpTypeVoid 
                                               %3 = OpTypeFunction %2 
                                               %6 = OpTypeFloat 32 
                                               %7 = OpTypeVector %6 4 
                                               %8 = OpTypePointer Private %7 
                                Private f32_4* %9 = OpVariable Private 
                                              %10 = OpTypePointer Input %7 
                                 Input f32_4* %11 = OpVariable Input 
                                              %13 = OpTypeInt 32 0 
                                          u32 %14 = OpConstant 128 
                                              %15 = OpTypeArray %6 %14 
                                              %16 = OpTypeStruct %7 %7 %7 %6 %6 %6 %15 %6 
                                              %17 = OpTypePointer Uniform %16 
Uniform struct {f32_4; f32_4; f32_4; f32; f32; f32; f32[128]; f32;}* %18 = OpVariable Uniform 
                                              %19 = OpTypeInt 32 1 
                                          i32 %20 = OpConstant 0 
                                              %21 = OpTypePointer Uniform %7 
                               Private f32_4* %25 = OpVariable Private 
                                          i32 %27 = OpConstant 1 
                               Private f32_4* %31 = OpVariable Private 
                                          i32 %33 = OpConstant 2 
                                              %37 = OpTypePointer Private %6 
                                 Private f32* %38 = OpVariable Private 
                                              %39 = OpTypeVector %6 2 
                                              %40 = OpTypePointer Input %39 
                        Input f32_2* vs_TEXCOORD0 = OpVariable Input 
                                          u32 %42 = OpConstant 0 
                                              %43 = OpTypePointer Input %6 
                                          i32 %46 = OpConstant 7 
                                              %47 = OpTypePointer Uniform %6 
                                              %53 = OpTypePointer Private %13 
                                 Private u32* %54 = OpVariable Private 
                                              %57 = OpTypeBool 
                                              %58 = OpTypeVector %57 2 
                                              %59 = OpTypePointer Private %58 
                              Private bool_2* %60 = OpVariable Private 
                                          i32 %61 = OpConstant 3 
                                          i32 %64 = OpConstant 4 
                                          i32 %72 = OpConstant 6 
                                              %78 = OpTypeVector %57 4 
                                          u32 %81 = OpConstant 1 
                                              %82 = OpTypePointer Private %57 
                                          f32 %97 = OpConstant 3.674022E-40 
                                Private f32* %100 = OpVariable Private 
                                         u32 %108 = OpConstant 3 
                                Private f32* %113 = OpVariable Private 
                             Private bool_2* %122 = OpVariable Private 
                                Private f32* %131 = OpVariable Private 
                                         f32 %134 = OpConstant 3.674022E-40 
                                         f32 %136 = OpConstant 3.674022E-40 
                                         f32 %140 = OpConstant 3.674022E-40 
                             Private bool_2* %142 = OpVariable Private 
                                             %149 = OpTypePointer Function %6 
                                         f32 %164 = OpConstant 3.674022E-40 
                                         i32 %170 = OpConstant 5 
                                         f32 %173 = OpConstant 3.674022E-40 
                                       f32_4 %194 = OpConstantComposite %136 %136 %136 %136 
                                       f32_2 %203 = OpConstantComposite %173 %173 
                                             %240 = OpTypePointer Function %7 
                                         f32 %262 = OpConstant 3.674022E-40 
                                       f32_2 %263 = OpConstantComposite %262 %136 
                                         f32 %269 = OpConstant 3.674022E-40 
                                         f32 %285 = OpConstant 3.674022E-40 
                                Private f32* %290 = OpVariable Private 
                              Private f32_4* %317 = OpVariable Private 
                                             %318 = OpTypeImage %6 Dim2D 0 0 0 1 Unknown 
                                             %319 = OpTypePointer UniformConstant %318 
        UniformConstant read_only Texture2D* %320 = OpVariable UniformConstant 
                                             %322 = OpTypeSampler 
                                             %323 = OpTypePointer UniformConstant %322 
                    UniformConstant sampler* %324 = OpVariable UniformConstant 
                                             %326 = OpTypeSampledImage %318 
                                             %333 = OpTypePointer Output %7 
                               Output f32_4* %334 = OpVariable Output 
                                             %335 = OpTypeVector %6 3 
                                             %345 = OpTypePointer Output %6 
                                          void %4 = OpFunction None %3 
                                               %5 = OpLabel 
                               Function f32* %150 = OpVariable Function 
                               Function f32* %161 = OpVariable Function 
                             Function f32_4* %241 = OpVariable Function 
                             Function f32_4* %251 = OpVariable Function 
                               Function f32* %293 = OpVariable Function 
                               Function f32* %308 = OpVariable Function 
                                        f32_4 %12 = OpLoad %11 
                               Uniform f32_4* %22 = OpAccessChain %18 %20 
                                        f32_4 %23 = OpLoad %22 
                                        f32_4 %24 = OpFMul %12 %23 
                                                      OpStore %9 %24 
                                        f32_4 %26 = OpLoad %11 
                               Uniform f32_4* %28 = OpAccessChain %18 %27 
                                        f32_4 %29 = OpLoad %28 
                                        f32_4 %30 = OpFMul %26 %29 
                                                      OpStore %25 %30 
                                        f32_4 %32 = OpLoad %11 
                               Uniform f32_4* %34 = OpAccessChain %18 %33 
                                        f32_4 %35 = OpLoad %34 
                                        f32_4 %36 = OpFMul %32 %35 
                                                      OpStore %31 %36 
                                   Input f32* %44 = OpAccessChain vs_TEXCOORD0 %42 
                                          f32 %45 = OpLoad %44 
                                 Uniform f32* %48 = OpAccessChain %18 %46 
                                          f32 %49 = OpLoad %48 
                                          f32 %50 = OpFMul %45 %49 
                                                      OpStore %38 %50 
                                          f32 %51 = OpLoad %38 
                                          f32 %52 = OpExtInst %1 8 %51 
                                                      OpStore %38 %52 
                                          f32 %55 = OpLoad %38 
                                          u32 %56 = OpConvertFToU %55 
                                                      OpStore %54 %56 
                                 Uniform f32* %62 = OpAccessChain %18 %61 
                                          f32 %63 = OpLoad %62 
                                 Uniform f32* %65 = OpAccessChain %18 %64 
                                          f32 %66 = OpLoad %65 
                                 Uniform f32* %67 = OpAccessChain %18 %61 
                                          f32 %68 = OpLoad %67 
                                 Uniform f32* %69 = OpAccessChain %18 %61 
                                          f32 %70 = OpLoad %69 
                                        f32_4 %71 = OpCompositeConstruct %63 %66 %68 %70 
                                          u32 %73 = OpLoad %54 
                                          i32 %74 = OpBitcast %73 
                                 Uniform f32* %75 = OpAccessChain %18 %72 %74 
                                          f32 %76 = OpLoad %75 
                                        f32_4 %77 = OpCompositeConstruct %76 %76 %76 %76 
                                       bool_4 %79 = OpFOrdLessThan %71 %77 
                                       bool_2 %80 = OpVectorShuffle %79 %79 0 1 
                                                      OpStore %60 %80 
                                Private bool* %83 = OpAccessChain %60 %81 
                                         bool %84 = OpLoad %83 
                                        f32_4 %85 = OpLoad %25 
                                        f32_4 %86 = OpLoad %31 
                                       bool_4 %87 = OpCompositeConstruct %84 %84 %84 %84 
                                        f32_4 %88 = OpSelect %87 %85 %86 
                                                      OpStore %25 %88 
                                Private bool* %89 = OpAccessChain %60 %42 
                                         bool %90 = OpLoad %89 
                                        f32_4 %91 = OpLoad %9 
                                        f32_4 %92 = OpLoad %25 
                                       bool_4 %93 = OpCompositeConstruct %90 %90 %90 %90 
                                        f32_4 %94 = OpSelect %93 %91 %92 
                                                      OpStore %9 %94 
                                   Input f32* %95 = OpAccessChain vs_TEXCOORD0 %81 
                                          f32 %96 = OpLoad %95 
                                          f32 %98 = OpFMul %96 %97 
                                 Private f32* %99 = OpAccessChain %25 %42 
                                                      OpStore %99 %98 
                                Private f32* %101 = OpAccessChain %25 %42 
                                         f32 %102 = OpLoad %101 
                                         u32 %103 = OpLoad %54 
                                         i32 %104 = OpBitcast %103 
                                Uniform f32* %105 = OpAccessChain %18 %72 %104 
                                         f32 %106 = OpLoad %105 
                                         f32 %107 = OpFDiv %102 %106 
                                                      OpStore %100 %107 
                                Private f32* %109 = OpAccessChain %9 %108 
                                         f32 %110 = OpLoad %109 
                                         f32 %111 = OpLoad %100 
                                         f32 %112 = OpFMul %110 %111 
                                                      OpStore %100 %112 
                                  Input f32* %114 = OpAccessChain vs_TEXCOORD0 %81 
                                         f32 %115 = OpLoad %114 
                                         f32 %116 = OpFNegate %115 
                                         u32 %117 = OpLoad %54 
                                         i32 %118 = OpBitcast %117 
                                Uniform f32* %119 = OpAccessChain %18 %72 %118 
                                         f32 %120 = OpLoad %119 
                                         f32 %121 = OpFAdd %116 %120 
                                                      OpStore %113 %121 
                                         u32 %123 = OpLoad %54 
                                         i32 %124 = OpBitcast %123 
                                Uniform f32* %125 = OpAccessChain %18 %72 %124 
                                         f32 %126 = OpLoad %125 
                                  Input f32* %127 = OpAccessChain vs_TEXCOORD0 %81 
                                         f32 %128 = OpLoad %127 
                                        bool %129 = OpFOrdLessThan %126 %128 
                               Private bool* %130 = OpAccessChain %122 %42 
                                                      OpStore %130 %129 
                                Uniform f32* %132 = OpAccessChain %18 %46 
                                         f32 %133 = OpLoad %132 
                                         f32 %135 = OpFAdd %133 %134 
                                                      OpStore %131 %135 
                                         f32 %137 = OpLoad %131 
                                         f32 %138 = OpFDiv %136 %137 
                                                      OpStore %131 %138 
                                         f32 %139 = OpLoad %131 
                                         f32 %141 = OpFMul %139 %140 
                                                      OpStore %131 %141 
                                         f32 %143 = OpLoad %131 
                                         f32 %144 = OpLoad %113 
                                        bool %145 = OpFOrdLessThan %143 %144 
                               Private bool* %146 = OpAccessChain %142 %42 
                                                      OpStore %146 %145 
                               Private bool* %147 = OpAccessChain %142 %42 
                                        bool %148 = OpLoad %147 
                                                      OpSelectionMerge %152 None 
                                                      OpBranchConditional %148 %151 %154 
                                             %151 = OpLabel 
                                         f32 %153 = OpLoad %100 
                                                      OpStore %150 %153 
                                                      OpBranch %152 
                                             %154 = OpLabel 
                                Private f32* %155 = OpAccessChain %9 %108 
                                         f32 %156 = OpLoad %155 
                                                      OpStore %150 %156 
                                                      OpBranch %152 
                                             %152 = OpLabel 
                                         f32 %157 = OpLoad %150 
                                Private f32* %158 = OpAccessChain %25 %42 
                                                      OpStore %158 %157 
                               Private bool* %159 = OpAccessChain %122 %42 
                                        bool %160 = OpLoad %159 
                                                      OpSelectionMerge %163 None 
                                                      OpBranchConditional %160 %162 %165 
                                             %162 = OpLabel 
                                                      OpStore %161 %164 
                                                      OpBranch %163 
                                             %165 = OpLabel 
                                Private f32* %166 = OpAccessChain %25 %42 
                                         f32 %167 = OpLoad %166 
                                                      OpStore %161 %167 
                                                      OpBranch %163 
                                             %163 = OpLabel 
                                         f32 %168 = OpLoad %161 
                                Private f32* %169 = OpAccessChain %9 %108 
                                                      OpStore %169 %168 
                                Uniform f32* %171 = OpAccessChain %18 %170 
                                         f32 %172 = OpLoad %171 
                                         f32 %174 = OpFAdd %172 %173 
                                                      OpStore %38 %174 
                                         f32 %175 = OpLoad %38 
                                  Input f32* %176 = OpAccessChain vs_TEXCOORD0 %81 
                                         f32 %177 = OpLoad %176 
                                        bool %178 = OpFOrdLessThan %175 %177 
                               Private bool* %179 = OpAccessChain %122 %42 
                                                      OpStore %179 %178 
                                  Input f32* %180 = OpAccessChain vs_TEXCOORD0 %81 
                                         f32 %181 = OpLoad %180 
                                Uniform f32* %182 = OpAccessChain %18 %170 
                                         f32 %183 = OpLoad %182 
                                        bool %184 = OpFOrdLessThan %181 %183 
                               Private bool* %185 = OpAccessChain %60 %42 
                                                      OpStore %185 %184 
                               Private bool* %186 = OpAccessChain %122 %42 
                                        bool %187 = OpLoad %186 
                               Private bool* %188 = OpAccessChain %60 %42 
                                        bool %189 = OpLoad %188 
                                        bool %190 = OpLogicalAnd %187 %189 
                               Private bool* %191 = OpAccessChain %122 %42 
                                                      OpStore %191 %190 
                               Private bool* %192 = OpAccessChain %122 %42 
                                        bool %193 = OpLoad %192 
                                       f32_4 %195 = OpLoad %9 
                                      bool_4 %196 = OpCompositeConstruct %193 %193 %193 %193 
                                       f32_4 %197 = OpSelect %196 %194 %195 
                                                      OpStore %9 %197 
                                Uniform f32* %198 = OpAccessChain %18 %64 
                                         f32 %199 = OpLoad %198 
                                Uniform f32* %200 = OpAccessChain %18 %61 
                                         f32 %201 = OpLoad %200 
                                       f32_2 %202 = OpCompositeConstruct %199 %201 
                                       f32_2 %204 = OpFAdd %202 %203 
                                       f32_4 %205 = OpLoad %25 
                                       f32_4 %206 = OpVectorShuffle %205 %204 4 5 2 3 
                                                      OpStore %25 %206 
                                       f32_4 %207 = OpLoad %25 
                                       f32_4 %208 = OpVectorShuffle %207 %207 0 1 0 0 
                                       f32_2 %209 = OpLoad vs_TEXCOORD0 
                                       f32_4 %210 = OpVectorShuffle %209 %209 1 1 1 1 
                                      bool_4 %211 = OpFOrdLessThan %208 %210 
                                      bool_2 %212 = OpVectorShuffle %211 %211 0 1 
                                                      OpStore %122 %212 
                                       f32_2 %213 = OpLoad vs_TEXCOORD0 
                                       f32_4 %214 = OpVectorShuffle %213 %213 1 1 1 1 
                                Uniform f32* %215 = OpAccessChain %18 %64 
                                         f32 %216 = OpLoad %215 
                                Uniform f32* %217 = OpAccessChain %18 %61 
                                         f32 %218 = OpLoad %217 
                                Uniform f32* %219 = OpAccessChain %18 %64 
                                         f32 %220 = OpLoad %219 
                                Uniform f32* %221 = OpAccessChain %18 %61 
                                         f32 %222 = OpLoad %221 
                                       f32_4 %223 = OpCompositeConstruct %216 %218 %220 %222 
                                      bool_4 %224 = OpFOrdLessThan %214 %223 
                                      bool_2 %225 = OpVectorShuffle %224 %224 0 1 
                                                      OpStore %142 %225 
                               Private bool* %226 = OpAccessChain %122 %42 
                                        bool %227 = OpLoad %226 
                               Private bool* %228 = OpAccessChain %142 %42 
                                        bool %229 = OpLoad %228 
                                        bool %230 = OpLogicalAnd %227 %229 
                               Private bool* %231 = OpAccessChain %122 %42 
                                                      OpStore %231 %230 
                               Private bool* %232 = OpAccessChain %122 %81 
                                        bool %233 = OpLoad %232 
                               Private bool* %234 = OpAccessChain %142 %81 
                                        bool %235 = OpLoad %234 
                                        bool %236 = OpLogicalAnd %233 %235 
                               Private bool* %237 = OpAccessChain %122 %81 
                                                      OpStore %237 %236 
                               Private bool* %238 = OpAccessChain %122 %42 
                                        bool %239 = OpLoad %238 
                                                      OpSelectionMerge %243 None 
                                                      OpBranchConditional %239 %242 %246 
                                             %242 = OpLabel 
                              Uniform f32_4* %244 = OpAccessChain %18 %27 
                                       f32_4 %245 = OpLoad %244 
                                                      OpStore %241 %245 
                                                      OpBranch %243 
                                             %246 = OpLabel 
                                       f32_4 %247 = OpLoad %9 
                                                      OpStore %241 %247 
                                                      OpBranch %243 
                                             %243 = OpLabel 
                                       f32_4 %248 = OpLoad %241 
                                                      OpStore %9 %248 
                               Private bool* %249 = OpAccessChain %122 %81 
                                        bool %250 = OpLoad %249 
                                                      OpSelectionMerge %253 None 
                                                      OpBranchConditional %250 %252 %256 
                                             %252 = OpLabel 
                              Uniform f32_4* %254 = OpAccessChain %18 %20 
                                       f32_4 %255 = OpLoad %254 
                                                      OpStore %251 %255 
                                                      OpBranch %253 
                                             %256 = OpLabel 
                                       f32_4 %257 = OpLoad %9 
                                                      OpStore %251 %257 
                                                      OpBranch %253 
                                             %253 = OpLabel 
                                       f32_4 %258 = OpLoad %251 
                                                      OpStore %9 %258 
                                       f32_2 %259 = OpLoad vs_TEXCOORD0 
                                       f32_2 %260 = OpVectorShuffle %259 %259 0 0 
                                       f32_2 %261 = OpFNegate %260 
                                       f32_2 %264 = OpFAdd %261 %263 
                                       f32_4 %265 = OpLoad %25 
                                       f32_4 %266 = OpVectorShuffle %265 %264 4 5 2 3 
                                                      OpStore %25 %266 
                                Private f32* %267 = OpAccessChain %25 %81 
                                         f32 %268 = OpLoad %267 
                                         f32 %270 = OpFMul %268 %269 
                                Private f32* %271 = OpAccessChain %25 %81 
                                                      OpStore %271 %270 
                                Private f32* %272 = OpAccessChain %25 %42 
                                         f32 %273 = OpLoad %272 
                                         f32 %274 = OpFNegate %273 
                                         f32 %275 = OpFMul %274 %269 
                                         f32 %276 = OpFAdd %275 %136 
                                Private f32* %277 = OpAccessChain %25 %42 
                                                      OpStore %277 %276 
                                       f32_4 %278 = OpLoad %9 
                                       f32_2 %279 = OpVectorShuffle %278 %278 3 3 
                                       f32_4 %280 = OpLoad %25 
                                       f32_2 %281 = OpVectorShuffle %280 %280 0 1 
                                       f32_2 %282 = OpFMul %279 %281 
                                       f32_4 %283 = OpLoad %25 
                                       f32_4 %284 = OpVectorShuffle %283 %282 4 5 2 3 
                                                      OpStore %25 %284 
                                  Input f32* %286 = OpAccessChain vs_TEXCOORD0 %42 
                                         f32 %287 = OpLoad %286 
                                        bool %288 = OpFOrdLessThan %285 %287 
                               Private bool* %289 = OpAccessChain %122 %42 
                                                      OpStore %289 %288 
                               Private bool* %291 = OpAccessChain %122 %42 
                                        bool %292 = OpLoad %291 
                                                      OpSelectionMerge %295 None 
                                                      OpBranchConditional %292 %294 %298 
                                             %294 = OpLabel 
                                Private f32* %296 = OpAccessChain %25 %81 
                                         f32 %297 = OpLoad %296 
                                                      OpStore %293 %297 
                                                      OpBranch %295 
                                             %298 = OpLabel 
                                Private f32* %299 = OpAccessChain %9 %108 
                                         f32 %300 = OpLoad %299 
                                                      OpStore %293 %300 
                                                      OpBranch %295 
                                             %295 = OpLabel 
                                         f32 %301 = OpLoad %293 
                                                      OpStore %290 %301 
                                  Input f32* %302 = OpAccessChain vs_TEXCOORD0 %42 
                                         f32 %303 = OpLoad %302 
                                        bool %304 = OpFOrdLessThan %303 %262 
                               Private bool* %305 = OpAccessChain %122 %42 
                                                      OpStore %305 %304 
                               Private bool* %306 = OpAccessChain %122 %42 
                                        bool %307 = OpLoad %306 
                                                      OpSelectionMerge %310 None 
                                                      OpBranchConditional %307 %309 %313 
                                             %309 = OpLabel 
                                Private f32* %311 = OpAccessChain %25 %42 
                                         f32 %312 = OpLoad %311 
                                                      OpStore %308 %312 
                                                      OpBranch %310 
                                             %313 = OpLabel 
                                         f32 %314 = OpLoad %290 
                                                      OpStore %308 %314 
                                                      OpBranch %310 
                                             %310 = OpLabel 
                                         f32 %315 = OpLoad %308 
                                Private f32* %316 = OpAccessChain %9 %108 
                                                      OpStore %316 %315 
                         read_only Texture2D %321 = OpLoad %320 
                                     sampler %325 = OpLoad %324 
                  read_only Texture2DSampled %327 = OpSampledImage %321 %325 
                                       f32_2 %328 = OpLoad vs_TEXCOORD0 
                                       f32_4 %329 = OpImageSampleImplicitLod %327 %328 
                                                      OpStore %317 %329 
                                       f32_4 %330 = OpLoad %9 
                                       f32_4 %331 = OpLoad %317 
                                       f32_4 %332 = OpFMul %330 %331 
                                                      OpStore %9 %332 
                                       f32_4 %336 = OpLoad %9 
                                       f32_3 %337 = OpVectorShuffle %336 %336 3 3 3 
                                       f32_4 %338 = OpLoad %9 
                                       f32_3 %339 = OpVectorShuffle %338 %338 0 1 2 
                                       f32_3 %340 = OpFMul %337 %339 
                                       f32_4 %341 = OpLoad %334 
                                       f32_4 %342 = OpVectorShuffle %341 %340 4 5 6 3 
                                                      OpStore %334 %342 
                                Private f32* %343 = OpAccessChain %9 %108 
                                         f32 %344 = OpLoad %343 
                                 Output f32* %346 = OpAccessChain %334 %108 
                                                      OpStore %346 %344 
                                                      OpReturn
                                                      OpFunctionEnd
"
}
}
Program "fp" {
SubProgram "gles3 " {
""
}
SubProgram "vulkan " {
""
}
SubProgram "gles3 " {
Keywords { "PIXELSNAP_ON" }
""
}
SubProgram "vulkan " {
Keywords { "PIXELSNAP_ON" }
""
}
}
}
}
}