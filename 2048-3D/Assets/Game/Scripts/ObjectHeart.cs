using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHeart : MonoBehaviour
{

    [SerializeField] private TrailRenderer m_trailRenderer;
    [SerializeField] private SpriteRenderer m_spriteRenderer;
    [SerializeField] private Rigidbody m_rigidbody;
    [SerializeField] private float m_speedMovement = 0;
    [SerializeField] private float m_speedMoveTop = 0;

    private bool m_isTrigger = false;

    private bool m_isMove = false;
    private bool m_isMoveTop = false;

    private bool m_isWallLeft = false;
    private bool m_isWallRight = false;

    private float m_distanceRay = 0;
    private RaycastHit m_hit;
    private Vector3 m_targetPosHit = new Vector3(0, 0.2f, 1);
    private void OnDestroy()
    {
        CubeMove.move -= OnMoveLeftRight;
        CubeMove.moveTop -= OnMoveTop;
        //transform.DOKill();
    }
    private void OnEnable()
    {
        m_isMove = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y - 0.8f, transform.position.z), transform.TransformDirection(m_targetPosHit), out m_hit, 20f))
        {
            if (m_hit.collider.tag == "wall-top" || m_hit.collider.tag == "cube")
            {
                m_distanceRay = m_hit.distance;
                m_spriteRenderer.size = new Vector2(0.7f, m_distanceRay * 0.016f);

            }

        }
    }
    private void FixedUpdate()
    {
        if (m_isMoveTop)
            m_rigidbody.velocity = new Vector3(0, 0, m_speedMoveTop * Time.deltaTime);
    }
    public void Init()
    {
        m_isMove = true;
        CubeMove.move += OnMoveLeftRight;
        CubeMove.moveTop += OnMoveTop;
        m_spriteRenderer.gameObject.SetActive(true);
    }

    private void OnMoveTop()
    {
        CubeMove.move -= OnMoveLeftRight;
        CubeMove.moveTop -= OnMoveTop;
        //
        m_trailRenderer.enabled = true;
        m_isMoveTop = true;
        m_isMove = false;
        m_spriteRenderer.gameObject.SetActive(false);
        m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    private void OnMoveLeftRight(float valueMove)
    {
        if (!m_isMove)
            return;
        //float x = Input.GetAxisRaw("Horizontal");
        valueMove *= Time.deltaTime * m_speedMovement;
        if (m_isWallLeft && valueMove < 0 || m_isWallRight && valueMove > 0) // check collider wall-left-right
            valueMove = 0;
        //
        m_rigidbody.MovePosition(new Vector3(transform.position.x + valueMove, transform.position.y, transform.position.z));

    }

  

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "wall-right")
        {

            m_isWallRight = true;
        }
        if (collider.tag == "wall-left")
        {
            m_isWallLeft = true;

        }
        if(collider.tag == "cube")
        {
            m_isMoveTop = false;
            m_rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            m_trailRenderer.enabled = false;
            //
            Cube cube = collider.GetComponent<Cube>();
            if (cube != null && !m_isTrigger)
            {
                m_isTrigger = true;
                GameController.UpdateLevelObjectCube(cube);
            }
            Destroy(gameObject, 0.1f);
        }
        else if (collider.tag == "wall-top")
        {
            m_isMoveTop = false;
            m_rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            m_trailRenderer.enabled = false;
            //
            Destroy(gameObject, 0.1f);
            //StartCoroutine(WaitSetupDestroy());
        }

    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "wall-right")
        {

            m_isWallRight = false;
        }
        if (collider.tag == "wall-left")
        {
            m_isWallLeft = false;
        }

    }
}
