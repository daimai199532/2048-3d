using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using DG.Tweening;

public class CubeMove : MonoBehaviour
{
    public static Action<float> move;
    public static Action moveTop;
    //
    [SerializeField] private TrailRenderer m_trailRenderer;
    [SerializeField] private SpriteRenderer m_spriteRenderer;
    [SerializeField] private Cube m_cube;
    [SerializeField] private Rigidbody m_rigidbody;
    [SerializeField] private float m_speedMovement = 0;
    [SerializeField] private float m_speedMoveTop = 0;
    private bool m_isMove=false;
    private bool m_isMoveTop = false;

    private bool m_isWallLeft = false;
    private bool m_isWallRight = false;

    private float m_distanceRay = 0;
    private RaycastHit m_hit;
    private Vector3 startPos = new Vector3(0, 0.2f, 1);
    private void OnDestroy()
    {
        move -= OnMoveLeftRight;
        moveTop -= OnMoveTop;
        transform.DOKill();
    }
    private void OnEnable()
    {
        m_isMove = false;
    }
    private void Update()
    {
        if (m_isMoveTop)
            return;
        //if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 20f))
        if (Physics.Raycast(new Vector3( transform.position.x,transform.position.y - 0.8f, transform.position.z), transform.TransformDirection(startPos), out m_hit, 20f))
            {
            if (m_hit.collider.tag == "wall-top" || m_hit.collider.tag == "cube")
            {
                m_distanceRay = m_hit.distance;
                m_spriteRenderer.size = new Vector2(0.7f,m_distanceRay * 0.016f);
                //Debug.DrawLine
            }

        }
    }
  

    public void Init()
    {
        m_isMove = true;
        move += OnMoveLeftRight;
        moveTop += OnMoveTop;
        m_spriteRenderer.gameObject.SetActive(true);
    }

    private void OnMoveTop()
    {
        move -= OnMoveLeftRight;
        moveTop -= OnMoveTop;
        //
        m_trailRenderer.enabled = true;
        m_isMoveTop = true;
        m_isMove = false;
        m_spriteRenderer.gameObject.SetActive(false);
        //m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        m_rigidbody.AddForce(new Vector3(0, 0, m_speedMoveTop), ForceMode.Impulse);
        //

    }
   
    private void OnMoveLeftRight(float valueMove)
    {
        if (!m_isMove)
            return;
        //float x = Input.GetAxisRaw("Horizontal");
        valueMove *= Time.deltaTime * m_speedMovement;
        if (m_isWallLeft && valueMove < 0 || m_isWallRight && valueMove > 0) // check collider wall-left-right
            valueMove = 0;
        //
        m_rigidbody.MovePosition(new Vector3(transform.position.x + valueMove, transform.position.y, transform.position.z));
       
    }
    public bool GetControll()
    {
        return m_isMove;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "wall-right")
        {

            m_isWallRight = true;
        }
        if(collider.tag == "wall-left")
        {
            m_isWallLeft = true;
 
        }
        if (collider.tag == "wall-top" || collider.tag == "cube")
        {
            m_isMoveTop = false;
            //m_rigidbody.constraints = RigidbodyConstraints.None;
            m_trailRenderer.enabled = false;
            //
            GameController.AddNewObjectCube(m_cube, true, true);
        }
    }
    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "wall-right")
        {
            
            m_isWallRight = false;
        }
        if (collider.tag == "wall-left")
        {
            m_isWallLeft = false;
        }
       
    }
}
