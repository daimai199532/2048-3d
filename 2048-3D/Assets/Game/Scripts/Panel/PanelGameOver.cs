using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;

public class PanelGameOver : MonoBehaviour
{
    [SerializeField] private Image m_title;
    [SerializeField] private Image m_icon;
    [SerializeField] private RectTransform m_objDecorText;
    [SerializeField] private RectTransform m_objScore;
    [SerializeField] private RectTransform m_objHightScore;
    [SerializeField] private GameObject m_bntKeepPlaying;
    [SerializeField] private GameObject m_bntRestart;
    [SerializeField] private TextMeshProUGUI m_textScore;
    [SerializeField] private TextMeshProUGUI m_textHighScore;
    [SerializeField] private TextMeshProUGUI m_textCountCube;
    [SerializeField] private TextMeshProUGUI m_textHighCountCube;

    private void OnEnable()
    {
        SetDefault();
    }
    // Start is called before the first frame update
  
    public void Init()
    {
        OnAnim();
    }
    private void SetDefault()
    {
        m_title.GetComponent<RectTransform>().DOAnchorPos3DY(-50, 0.01f);
        m_title.DOFade(0, 0.01f);
        m_title.gameObject.transform.DOScale(0, 0.01f);
        m_icon.DOFade(0, 0.01f);
        m_objDecorText.DOAnchorPos3DX(800f, 0.01f);
        m_objScore.DOAnchorPos3DX(800f, 0.01f);
        m_objHightScore.DOAnchorPos3DX(800f, 0.01f);
        //m_bntKeepPlaying.transform.DOScale(0, 0.01f);
        //m_bntRestart.transform.DOScale(0, 0.01f);
        //
        m_textScore.text = MainModel.scoreLevel.ToString();
        m_textHighScore.text = MainModel.highScore.ToString();
        m_textCountCube.text = MainModel.countCubelLevel.ToString();
        m_textHighCountCube.text = MainModel.highCountCube.ToString();
    }
    private void OnAnim()
    {
        Sequence seq = DOTween.Sequence();
        //seq.SetDelay(0.5f);
        seq.Append(m_title.GetComponent<RectTransform>().DOAnchorPos3DY(-90, 0.5f));
        seq.Join(m_title.DOFade(1, 0.7f).SetEase(Ease.OutBounce));
        seq.Join(m_title.gameObject.transform.DOScale(0.5f, 0.01f));
        //
        seq.Append(m_icon.DOFade(1f, 0.5f).OnComplete(()=>{
            m_icon.DOFade(0.3f, 0.25f).OnComplete(() => {
                m_icon.DOFade(1f, 0.1f);
            }).SetLoops(2);
        }));
        //
        seq.Append(m_objDecorText.DOAnchorPos3DX(0,0.5f).SetEase(Ease.OutBounce));
        seq.Join(m_objScore.DOAnchorPos3DX(0, 1f).SetEase(Ease.OutBounce));
        seq.Join(m_objHightScore.DOAnchorPos3DX(0, 1.5f).SetEase(Ease.OutBounce));

        seq.Append(m_bntKeepPlaying.transform.DOScale(1, 0.5f).SetEase(Ease.OutBounce));
        seq.Append(m_bntRestart.transform.DOScale(1, 0.5f).SetEase(Ease.OutBounce));

    }
    public void RestartOnClick()
    {
        SceneManager.LoadScene(0);
    }
    public void NextGameOnClick()
    {
        gameObject.SetActive(false);
    }
}
