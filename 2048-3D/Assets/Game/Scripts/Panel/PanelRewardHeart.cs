using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class PanelRewardHeart : MonoBehaviour
{
    [SerializeField] private Image m_bg;
    [SerializeField] private List<Image> m_listImage;
    private void OnEnable()
    {
        SetDefault();
    }
    public void Init()
    {
        OnAnimOpen();
    }
    private void OnAnimOpen()
    {
    
        Sequence seq = DOTween.Sequence();
        seq.AppendCallback(() => {
            m_bg.gameObject.SetActive(true);
        });
        seq.Append(m_bg.DOFade(1, 0.5f));
        foreach(Image item in m_listImage)
            seq.Join(item.DOFade(1, 0.5f));
        //
        seq.Join(m_bg.GetComponent<RectTransform>().DOAnchorPos3DY(0, 0.5f));
        seq.SetDelay(1f);
        seq.Play();
    }
    private void OnAnimClose()
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(m_bg.DOFade(0, 0.5f));
        foreach (Image item in m_listImage)
            seq.Join(item.DOFade(0, 0.4f));
        seq.Join(m_bg.GetComponent<RectTransform>().DOAnchorPos3DY(-100f, 0.5f));
        seq.OnComplete(() => {
            transform.gameObject.SetActive(false);
        });
    }
    private void SetDefault()
    {
        m_bg.gameObject.SetActive(false);
        m_bg.DOFade(0, 0.01f);
        foreach (Image item in m_listImage)
            item.DOFade(0, 0.01f);
        m_bg.GetComponent<RectTransform>().DOAnchorPos3DY(100f, 0.01f);
       
    }
    public void ClaimOnClick()
    {
        GameController.CreateObjHeart();
        OnAnimClose();
        
    }
    public void NothankOnClick()
    {
        OnAnimClose();

    }
    public void CloseOnClick()
    {
        OnAnimClose();

    }
}
