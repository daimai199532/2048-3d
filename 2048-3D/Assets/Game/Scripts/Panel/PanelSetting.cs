using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PanelSetting : MonoBehaviour
{
    [SerializeField] private Toggle m_music;
    [SerializeField] private Toggle m_sound;
    private void Start()
    {
        m_music.isOn = SoundManager.IsMusicOn();
        m_sound.isOn = SoundManager.IsSoundOn();
    }
    private void OnEnable()
    {
        Time.timeScale = 0;
    }
    private void OnDisable()
    {
        Time.timeScale = 1;
    }
    public void OnRestart()
    {
        SceneManager.LoadScene(0);
    }
    public void OnClose()
    {
        gameObject.SetActive(false);
    }
    public void SoundOnclick()
    {
        //SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        SoundManager.MuteSound(!m_sound.isOn);
       
    }
    public void MusicOnclick()
    {
        //SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        SoundManager.MuteMusic(!m_music.isOn);
       
    }
    public void MiniGameOnClick()
    {
        SceneManager.LoadScene(1);
    }
}
