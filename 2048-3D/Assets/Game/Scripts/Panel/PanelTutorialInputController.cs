using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;


public class PanelTutorialInputController : MonoBehaviour
{
    [SerializeField] GameObject m_bg;
    [SerializeField] TextMeshProUGUI m_textTime;
    public void Init()
    {
        StartCoroutine(WaitCompleteInit());
    }
    IEnumerator WaitCompleteInit()
    {
        m_bg.transform.DOScale(1, 1f).SetDelay(1f).SetEase(Ease.OutBack);
        //
       
        int time = 6;
        m_textTime.text = time.ToString();
        m_textTime.transform.DOScale(1,0.5f).SetDelay(2f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(1f);
        while(time > 0)
        {
            yield return new WaitForSeconds(1f);
            time--;
            m_textTime.text = time.ToString();
        }
        m_bg.transform.DOScale(0, 1f).SetDelay(1f).SetEase(Ease.OutSine);
        m_textTime.transform.DOScale(0, 0.5f).SetEase(Ease.OutSine);
        //m_textTime.gameObject.SetActive(false);
        yield return new WaitForSeconds(2f);
        GameController.PauseGame(false,false);
        gameObject.SetActive(false);
        
    }
}
