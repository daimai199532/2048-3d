using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class PanelRewardCubeCreate : MonoBehaviour
{
    [SerializeField] private Image m_objTitle;
    [SerializeField] private Image m_iconCube;
    [SerializeField] private Image m_iconCubeValue;
    [SerializeField] private TextMeshProUGUI m_textValueCube;
    [SerializeField] private GameObject m_decorLine;
    [SerializeField] private GameObject m_ObjReward;
    [SerializeField] private GameObject m_bntClaim;
    [SerializeField] private GameObject m_bntNothank;
    [SerializeField] private TextMeshProUGUI m_textTimeCountAds;
    [SerializeField] private Animation m_effectAmin;
    private void OnEnable()
    {
        //ChangeColor(6);
        GameController.PauseGame(true,false);
        SetDefault();

    }
    private void OnDisable()
    {
        GameController.PauseGame(false,false);
    }

    public void Init(int value, int color)
    {
        ChangeColor(color);
        ChangeText(value);
        OnAnim();
    }
  
    private void SetDefault()
    {
        //Transform position = m_objTitle.GetComponent<RectTransform>();
        m_objTitle.DOFade(0, 0.0001f);
        m_objTitle.GetComponent<RectTransform>().DOAnchorPos3DY(0, 0.0001f);
        m_iconCube.transform.DOScale(0, 0.0001f);
        m_decorLine.GetComponent<RectTransform>().DOAnchorPos3DX(600f, 0.0001f);
        m_ObjReward.transform.DOScale(0, 0.0001f);
        m_bntClaim.transform.DOScale(0, 0.0001f);
        m_bntNothank.transform.DOScale(0, 0.0001f);
        //
        m_objTitle.gameObject.SetActive(false);
        m_iconCube.gameObject.SetActive(false);
        m_decorLine.gameObject.SetActive(false);
        m_ObjReward.gameObject.SetActive(false);
        m_bntClaim.gameObject.SetActive(false);
        m_bntNothank.gameObject.SetActive(false);

    }
    
    private void ChangeColor(int color)
    {
        switch (color)
        {
            case 0:
                m_iconCubeValue.color = Color.green;
                break;
            case 1:
                m_iconCubeValue.color = Color.red;
                break;
            case 2:
                m_iconCubeValue.color = Color.blue;
                break;
            case 3:
                m_iconCubeValue.color = ConVertColor(200f, 150f, 16f, 255f);//new Color((float)200/255, (float)150 /255, (float)16 /255,1f);
                break;
            case 4:
                m_iconCubeValue.color = ConVertColor(150f, 0f, 255f, 255f);//new Color(150,0,255,255);
                break;
            case 5:
                m_iconCubeValue.color = ConVertColor(255f, 255f, 0f, 255f);// new Color(255,255,0,255);
                break;
            case 6:
                m_iconCubeValue.color = ConVertColor(255f, 200f, 225f, 255f); //new Color(255, 200, 225, 255);
                break;
            case 7:
                m_iconCubeValue.color = ConVertColor(150f, 255f, 225f, 255f); //new Color(150, 255, 225, 255);
                break;
            case 8:
                m_iconCubeValue.color = Color.black;
                break;
        }
    }
    private Color ConVertColor(float x, float y, float z, float v)
    {
        x = x / 255;
        y = y / 255;
        z = z / 255;
        v = v / 255;
        return new Color(x, y, z, v);
    }
    private void ChangeText(int value)
    {
        float sizeTextValue = 0;
        int count = 0;
        int index = value;
        while (index % 10 > 0)
        {
            count++;
            index = index / 10;
        }
        //Debug.Log(count + "==================");
        switch (count)
        {
            case 1:
                sizeTextValue = 140f;
                break;
            case 2:
                sizeTextValue = 140f;
                break;
            case 3:
                sizeTextValue = 95f;
                break;
            case 4:
                sizeTextValue = 77f;
                break;
            case 5:
                sizeTextValue = 63f;
                break;
        }
        //
        m_textValueCube.text = value.ToString();
        m_textValueCube.fontSize = sizeTextValue;
        //

    }
    private void OnAnim()
    {
        Sequence seq = DOTween.Sequence();
        seq.AppendCallback( ()=> {
            m_objTitle.gameObject.SetActive(true);
        });
        seq.Append(m_objTitle.DOFade(1, 1f));
        seq.Join(m_objTitle.GetComponent<RectTransform>().DOAnchorPos3DY(-100, 1f));
        //
        seq.AppendCallback(() => {
            m_iconCube.gameObject.SetActive(true);
        });
        seq.Append(m_iconCube.transform.DOScale(1f,0.3f));
        //
        seq.AppendCallback(() => {

            m_effectAmin.IsPlaying("EffectRotate");
            m_decorLine.gameObject.SetActive(true);
        });
        seq.Append(m_decorLine.GetComponent<RectTransform>().DOAnchorPos3DX(-100, 0.3f).SetEase(Ease.Linear));
        seq.Append(m_decorLine.GetComponent<RectTransform>().DOAnchorPos3DX(0, 0.2f).SetEase(Ease.Linear));
        //
        seq.AppendCallback(() =>
        {
            m_ObjReward.gameObject.SetActive(true);
        });
        seq.Append(m_ObjReward.transform.DOScale(1, 0.15f));
        //
        seq.AppendCallback(() =>
        {
            m_bntClaim.gameObject.SetActive(true);
        });
        seq.Append(m_bntClaim.transform.DOScale(1,  0.15f));
        //
        seq.AppendCallback(() =>
        {
            m_bntNothank.gameObject.SetActive(true);
        });
        seq.Append(m_bntNothank.transform.DOScale(1, 0.1f));
        seq.OnComplete(() => {
            StartCoroutine(TimeCountAds());
        });

    }
    IEnumerator TimeCountAds()
    {
        
        int timeCount = 30;
        m_textTimeCountAds.text = timeCount.ToString() +"S";
        m_textTimeCountAds.color = Color.red;
        yield return new WaitForSeconds(1f);
        while (timeCount > 0)
        {
            yield return new WaitForSeconds(1f);
            timeCount--;
            m_textTimeCountAds.text = timeCount.ToString() + "S";
            m_textTimeCountAds.color = Color.red;
        }
        yield return new WaitForSeconds(1f);
        m_textTimeCountAds.text = "Claim";
        m_textTimeCountAds.color = Color.white;
        m_bntClaim.GetComponent<Button>().interactable = true;
    }
    public void ClaimOnClick()
    {
        GameController.UpdateBoom(1);
        gameObject.SetActive(false);
        GameController.PauseGame(false,false);
    }
    public void NothankOnClick()
    {
        gameObject.SetActive(false);
        GameController.PauseGame(false,false);
    }
}
