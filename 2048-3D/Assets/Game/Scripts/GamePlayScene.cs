using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class GamePlayScene : MonoBehaviour
{
    [SerializeField] private Camera m_cam;
    [SerializeField] private List<TextMeshProUGUI> m_textCubes;
    [SerializeField] private PanelRewardCubeCreate m_panelRewardCube;
    [SerializeField] private PanelRewardHeart m_panelRewardHeart;
    [SerializeField] private PanelSetting m_panelSetting;
    [SerializeField] private PanelGameOver m_panelGameOver;
    [SerializeField] private PanelTutorialInputController m_panelTutorialStartGame;
    [SerializeField] private TextMeshProUGUI m_textScore;
    [SerializeField] private TextMeshProUGUI m_textBoom;
    private int m_countText=0;
    public List<Cube> m_listNewCube = new List<Cube>();
    private void Awake()
    {
        OnStartGame();
        //
        GameController.updateOpenPanelRewardCubeEvent += UpdateOpenPanelRewardCube;
        GameController.updateOpenPanelRewardHeart += UpdateOpenPanelRewardHeart;
        GameController.updateTextCubeEvent += UpdateTextCube;
        GameController.updateBoomEvent += UpdateBoom;
        GameController.updateScoreEvent += UpdateScore;
        GameController.addNewObjectCubeEvent += AddNewObjectCube;
        GameController.updateOpenPanelGameOverEvent += UpdateOpenPanelGameOver;
    }
    private void OnDestroy()
    {
        GameController.updateOpenPanelRewardCubeEvent -= UpdateOpenPanelRewardCube;
        GameController.updateOpenPanelRewardHeart -= UpdateOpenPanelRewardHeart;
        GameController.updateTextCubeEvent -= UpdateTextCube;
        GameController.updateBoomEvent -= UpdateBoom;
        GameController.updateScoreEvent -= UpdateScore;
        GameController.addNewObjectCubeEvent -= AddNewObjectCube;
        GameController.updateOpenPanelGameOverEvent -= UpdateOpenPanelGameOver;
    }
    private void OnStartGame()
    {
        Application.targetFrameRate = 60;
        //
        MainModel.LoadData();
        GetDeflaut();
        GameController.PauseGame(true,false);
        //
        m_panelTutorialStartGame.gameObject.SetActive(true);
        m_panelTutorialStartGame.Init();
    }
    private void UpdateScore(int value)
    {
        m_textScore.text = MainModel.scoreLevel.ToString();
    }
    private void UpdateBoom(int value)
    {
        m_textBoom.text = MainModel.boom.ToString();
    }
    private void UpdateOpenPanelRewardCube(int value, int color)
    {
        if(MainModel.statusGame == status.stop)
        {
            m_panelRewardCube.gameObject.SetActive(true);
            m_panelRewardCube.Init(value, color);
        }
    }
    private void UpdateOpenPanelGameOver()
    {
        if (MainModel.statusGame == status.gameOver)
        {
            m_panelGameOver.gameObject.SetActive(true);
            m_panelGameOver.Init();
        }
    }
    private void UpdateTextCube(Vector3 pos, int value)
    {
        
        m_countText ++;
        if (m_countText > 2)
            m_countText = 0;
        OnAnimTextCube(m_textCubes[m_countText],  pos, value);
       
    }
    private void OnAnimTextCube(TextMeshProUGUI obj, Vector3 pos, int value)
    {
        obj.transform.position = new Vector3(pos.x, pos.y + 2f, pos.z - 0.5f);
        obj.text = "+" + value.ToString();
        //Debug.Log("====Open========" + obj.gameObject.name);
        obj.gameObject.SetActive(true);
        obj.transform.DOMoveY(obj.transform.position.y + 2f, 1.5f).SetUpdate(true).OnComplete(() => {
            obj.gameObject.SetActive(false);
            //Debug.Log("====Close========" + obj.gameObject.name);
        });
    }    
    private void UpdateOpenPanelRewardHeart()
    {
        if(MainModel.statusGame == status.play)
        {
            m_panelRewardHeart.gameObject.SetActive(true);
            m_panelRewardHeart.Init();
        }
    }
    private void AddNewObjectCube(Cube cube, bool isAdd, bool isFirst)
    {
        
        if (isAdd)
        {
           
            if (!m_listNewCube.Contains(cube))
                m_listNewCube.Add(cube);
            if (!isFirst)
            {
                int value = cube.GetValue() * 2;
                Cube targetCube = SearchCube(value);
                if (targetCube != null && !cube.GetControll())
                    GameController.UpdateLevelObjectCube(cube, targetCube.transform.position, targetCube.gameObject);
                else
                    GameController.UpdateLevelObjectCube(cube, null);
            }
           
        }
        else
        {
            if (m_listNewCube.Contains(cube))
                m_listNewCube.Remove(cube);
            GameController.DestroyObjectCube(cube);
        }
     
    }
    private Cube SearchCube(int value)
    {
        if(m_listNewCube.Count > 0)
        {
            foreach(Cube cube in m_listNewCube)
            {
                if (cube.GetValue() == value)
                    return cube;
            }
        }
        return null;
    }
    
    public void HeartOnClick()
    {
        UpdateOpenPanelRewardHeart();
    }
    public void SettingOnClick()
    {
        m_panelSetting.gameObject.SetActive(true);
    }
    public void BoomOnClick()
    {
        if(MainModel.boom > 0)
        {
            GameController.UpdateBoom(-1);
            GameController.CreateObjBoom();
        }
    }
    private void GetDeflaut()
    {
        m_textBoom.text = MainModel.boom.ToString();
        m_textScore.text = MainModel.scoreLevel.ToString();
    }
}
