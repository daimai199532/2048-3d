using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class PanelMiniGameComplete : MonoBehaviour
{
    [SerializeField] RectTransform m_iconCube;
    [SerializeField] Image m_iconImgCube;
    [SerializeField] List<RectTransform> m_listIconStar = new List<RectTransform>();
    [SerializeField] RectTransform m_diamond;
    [SerializeField] RectTransform m_bntNext;
    [SerializeField] RectTransform m_iconDiamond;
    [SerializeField] TextMeshProUGUI m_textDiamond;
    [SerializeField] RectTransform m_pointFinalIconDiamond;
    private List<RectTransform> m_listIconDiamond = new List<RectTransform>();
    private int m_countDiamond = 0;
    private void Start()
    {
        Init();
    }
    public void Init()
    {
        m_textDiamond.text = "";

        Sequence seq = DOTween.Sequence();
        seq.SetDelay(1f);
        seq.Append(m_iconCube.DOScale(1, 0.5f));
        seq.Join(m_diamond.DOScale(1, 0.5f));
        seq.Join(m_bntNext.DOScale(1, 0.5f));
        //
        seq.AppendInterval(0.5f);
        seq.AppendCallback(() => {
            m_listIconStar[0].gameObject.SetActive(true);
        });
        seq.AppendInterval(0.5f);
        seq.AppendCallback(() => {
            m_listIconStar[1].gameObject.SetActive(true);
        });
        seq.AppendInterval(0.5f);
        seq.AppendCallback(() => {
            m_listIconStar[2].gameObject.SetActive(true);
        });
        RunDiamond();
    }
    private void RunDiamond()
    {
        // m_iconDiamond.SetParent(m_listIconStar[0].parent);
        //m_iconDiamond.anchoredPosition3D = m_listIconStar[0].anchoredPosition3D;
        StartCoroutine(CreateDiamond());
    }
    IEnumerator CreateDiamond()
    {
        int j = 0;
        int count = m_listIconStar.Count;
        for (int i = 0; i < 10; i++)
        {
            j = (i + count) % count;
            while (j > count)
            {
                j = (i + count) % count; 
            }
           
            RectTransform icon = Instantiate(m_iconDiamond);
            icon.SetParent(m_listIconStar[j].parent);
            icon.anchoredPosition3D = m_listIconStar[j].anchoredPosition3D;
            yield return new WaitForSeconds(0.1f);
            //icon.SetParent(m_listIconStar[0].parent.parent);
            //icon.gameObject.SetActive(true);
            m_listIconDiamond.Add(icon);
        }
        yield return new WaitForSeconds(2f);
        StartCoroutine(RunAnimDiamond());
    }
    IEnumerator RunAnimDiamond()
    {
        int count = m_listIconDiamond.Count;
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < count; i++) // set parent and active and scale
        {
            m_listIconDiamond[i].DOScale(0.3f, 0.01f);
            m_listIconDiamond[i].SetParent(m_pointFinalIconDiamond.parent);
            yield return new WaitForSeconds(0.1f);
            m_listIconDiamond[i].gameObject.SetActive(true);
        }
        yield return new WaitForSeconds(0.1f);
        for (int i = 0; i < count; i++) // move 
        {
            m_listIconDiamond[i].DOAnchorPos3D(m_pointFinalIconDiamond.anchoredPosition3D,0.2f).OnComplete(()=> {
                m_countDiamond++;
                m_textDiamond.text = "+" + m_countDiamond;
                m_listIconDiamond[i].gameObject.SetActive(false);
                m_pointFinalIconDiamond.DOScale(0.4f, 0.1f).OnComplete(() => {
                    m_pointFinalIconDiamond.DOScale(0.2f, 0.1f).OnComplete(() =>
                    {
                        m_pointFinalIconDiamond.DOScale(0.3f, 0.1f);
                    });
                });
                m_textDiamond.transform.DOScale(1.2f, 0.1f).OnComplete(() => {
                    m_textDiamond.transform.DOScale(0.8f, 0.1f).OnComplete(() =>
                    {
                        m_textDiamond.transform.DOScale(1f, 0.1f);
                    });
                });
            });
            yield return new WaitForSeconds(0.3f);
            
        }
    }    
    public void NextOnClick()
    {
        SceneManager.LoadScene(1);
    }
}
