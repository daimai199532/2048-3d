using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraTarget : MonoBehaviour
{
    [SerializeField] private Transform m_Box;
    [SerializeField] private Image m_bg;
    [SerializeField] private List<Color> m_listColor = new List<Color>();
    private void Awake()
    {
        GameController.updateCameraEvent += OnCamera;
        //
        ChangeColorBackGround();
    }
    private void OnDestroy()
    {
        GameController.updateCameraEvent -= OnCamera;
    }
    private void OnCamera(Vector3 pos, Vector3 rotate, bool isPlay)
    {
        if(isPlay)
        {
            transform.position = new Vector3(pos.x, pos.y, pos.z);
            transform.eulerAngles = new Vector3(rotate.x, rotate.y, 0);
        }
        else
        {
            transform.position = new Vector3(pos.x, pos.y, pos.z);
        }
    }
    // Update is called once per frame
    //void LateUpdate()
    //{
    //    transform.position = new Vector3(m_player.transform.position.x, m_player.transform.position.y, m_player.transform.position.z);
    //    transform.eulerAngles = new Vector3(m_player.transform.eulerAngles.x, m_player.transform.eulerAngles.y, 0);
    //}
    void ChangeColorBackGround()
    {
        int rand = Random.RandomRange(0, m_listColor.Count);
        Color temp = m_listColor[rand];
        m_bg.color =new Color(temp.r,temp.g,temp.b,150);
    }
}
