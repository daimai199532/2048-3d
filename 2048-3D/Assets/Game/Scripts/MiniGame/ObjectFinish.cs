using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class ObjectFinish : MonoBehaviour
{
    [SerializeField] private Animator m_objectSpringBoardAnim; 
    [SerializeField] private Transform m_objectSBoxJump;
    [SerializeField] private Rigidbody m_rbObjectSBoxJump;
    //
    //[SerializeField] private Transform m_cam;
    [SerializeField] private ObjectPlane m_prefabPlan;
    [SerializeField] private GameObject pointStartPlan;
    private List<ObjectPlane> listPoint = new List<ObjectPlane>();
    private bool m_isCameraFollow = false;
    private Animator m_objectSCubeAnim;
    private void Awake()
    {
        GameController.onStepCompleteLevelEvent += OnAnimCompleteGame;
        GameController.updateObjectFinish += UpdateObjectPlan;
    }
    private void OnDestroy()
    {
        GameController.onStepCompleteLevelEvent -= OnAnimCompleteGame;
        GameController.updateObjectFinish -= UpdateObjectPlan;
    }
    private void Start()
    {
        OnPlan(10, pointStartPlan.transform.position);
    }

    private void LateUpdate()
    {
        if (m_isCameraFollow)
            GameController.UpdateCamera(new Vector3(m_objectSBoxJump.transform.position.x, m_objectSBoxJump.transform.position.y, m_objectSBoxJump.transform.position.z - 5f), transform.eulerAngles, false);
        //m_cam.transform.position = new Vector3(m_objectSBoxJump.transform.position.x, m_objectSBoxJump.transform.position.y, m_objectSBoxJump.transform.position.z - 5f);
        //cam.transform.localPosition = objectSBoxJump.transform.localPosition;
    }
    private void UpdateObjectPlan(int color)
    {
        // neu player tang/giam so luong Plan (getColor > list.Count)
        int count = listPoint.Count;
        int temp = (color + 1) - count;

        if (color > count) // xoa luon cai cuoi roi them lai
        {
            //listPoint[listPoint.Count - 1].OnInit(pointStartPlan.transform.position, listPoint.Count - 1, true, false);
            OnPlan(temp, pointStartPlan.transform.position);
        }
        else
        {
            //Debug.Log("On remove");
            int tempCount = Mathf.Abs(temp);
            while (tempCount > 1)
            {
                tempCount--;
                listPoint[listPoint.Count - 1].UpdateDestroy();
                listPoint.RemoveAt(listPoint.Count - 1);
            }
            listPoint[listPoint.Count - 1].OnInit(pointStartPlan.transform.position, listPoint.Count - 1, listPoint.Count - 1, false);
        }
    }
    private void OnAnimCompleteGame(GameObject obj)
    {

        m_isCameraFollow = true;
        m_objectSCubeAnim = obj.GetComponent<Animator>();
        if (m_objectSCubeAnim == null)
        {
            Debug.Log("null object player");
            return;
        }
        StartCoroutine(WaitOnAnimCompleteGame());
    }
    IEnumerator WaitOnAnimCompleteGame()
    {
        //Debug.Log("On ====");
        float posY = m_objectSBoxJump.transform.position.y;// save vi tri ban dau cua BoxJump theo Y
        m_objectSCubeAnim.SetBool("isFinish", true);
        yield return new WaitForSeconds(1.55f);
        m_objectSpringBoardAnim.SetBool("isBoardTramplin", true);
        yield return new WaitForSeconds(0.85f);
        //
        OnJumpObjBox(posY);
    }
    private void OnJumpObjBox(float PosY)
    {
        Vector3 pos1 = m_objectSBoxJump.transform.position;// save position
        Vector3 pos3 = new Vector3(pos1.x, PosY, pointStartPlan.transform.position.z);//new Vector3(pos1.x, PosY, pos1.z + 5.5f);
        Vector3 pos2 = new Vector3(pos1.x, pos1.y + 5.5f, (pos1.z + pos3.z) / 2f);

        Vector3[] listPath = new Vector3[3] { pos1, pos2, pos3 };
        //

        m_objectSBoxJump.DOPath(listPath, 3f, PathType.CatmullRom).SetEase(Ease.Linear).OnComplete(() => {
            m_rbObjectSBoxJump.useGravity = enabled;
        });
        m_objectSBoxJump.DOLocalRotate(new Vector3(1800, 2600, 1800), 5f);

    }
    private void OnPlan(int valueAdd, Vector3 pointStartPlan) // create plan
    {
        int count = listPoint.Count;
        if (count > 0) // setTrigger cuoi thanh false
            listPoint[count - 1].OnInit(pointStartPlan, count - 1, count + valueAdd - 1, false);
        for (int i = count; i < count + valueAdd; i++)
        {
            ObjectPlane plan = Instantiate(m_prefabPlan);
            if (i == count + valueAdd - 1)
            {
                //Debug.Log(i + "=======+====" + (count + valueAdd - 1));
                plan.OnInit(pointStartPlan, i, count + valueAdd - 1, true);
            }
            else
                plan.OnInit(pointStartPlan, i, count + valueAdd - 1, true);

            plan.gameObject.SetActive(true);
            listPoint.Add(plan);

        }
    }
}
