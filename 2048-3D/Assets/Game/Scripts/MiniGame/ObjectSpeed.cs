using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpeed : MonoBehaviour
{

    //private void OnTriggerEnter(Collider collider)
    //{
    //    if (collider.gameObject.tag == "Player")
    //    {
           
    //        PlayerMovement player = collider.transform.parent.parent.GetComponent<PlayerMovement>();
    //        if (player == null)
    //            return;
           
    //    }
    //}
    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            //Debug.Log("=======================");
            PlayerMovement player = collider.transform.parent.parent.GetComponent<PlayerMovement>();
            //Debug.Log(player + "=======================");
            if (player == null)
                return;
            if(!player.GetIsDoubleCube())
                player.OnJumpPlayerCenter();
        }   
    }
}
