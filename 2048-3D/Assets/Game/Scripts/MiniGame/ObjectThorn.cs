using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectThorn : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
            PlayerMovement player = collider.transform.parent.parent.GetComponent<PlayerMovement>();
            if (player == null)
                return;
            player.OnHurt();
        }
    }
}
