using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ObjectPlane : MonoBehaviour
{
    [SerializeField] private List<MeshRenderer> m_listRenderer = new List<MeshRenderer>();
    [SerializeField] private TextMeshProUGUI m_textValue;
    [SerializeField] private GameObject m_planeChild;
    [SerializeField] private Collider m_collider;
    [SerializeField] private GameObject m_planeRoll;
    public bool m_isTrigger = true;
    public int m_index = 0;
    private int m_value;
    private int m_color;
    private float m_sizeTextValue;
   
    public void OnInit(Vector3 startPoint, int index, int ValueCountList,bool isAddNew)
    {
        m_index = index;
        if (isAddNew)
        {
            SetPosition(startPoint, m_index);
            OnSetDefault();
        }
        if(ValueCountList > m_index)
        {
            m_collider.isTrigger = true;
            m_isTrigger = true;
        }
        else
        {
            m_collider.isTrigger = false;
            m_isTrigger = false;
        }
        //m_isTrigger = isLastList;
        //if (isLastList)
        //    m_collider.isTrigger = true;
        //else
        //    m_collider.isTrigger = false;
    }
   
    public void UpdateDestroy()
    {
        Destroy(gameObject);
        //Debug.Log("Ondestroy");
    }
    private void SetPosition(Vector3 startPoint, int index) // index = vi ti trong list
    {
        //Debug.Log(m_index+ "==========="+ index);
        transform.position = new Vector3(startPoint.x, startPoint.y - (7 * index ),startPoint.z);
        //if(isLastList)
        //    transform.position = new Vector3(startPoint.x, startPoint.y - 7 * index - 0.25f, startPoint.z);
    }
    private void OnSetDefault()
    {
        m_color = m_index + 1;
        m_value = (int)Mathf.Pow(2f, m_color);
        ChangeColor(m_color);
        ChangeText(m_value);
    }
    private void ChangeColor(int color)
    {
        Color sample = Color.black;
       
        int tempColor = color % 9;
        switch (tempColor)
        {
            case 0:
                sample = ConVertColor(255f, 240f, 130f, 255f);
                //sample = Color.green;

                break;
            case 1:

                sample = ConVertColor(190f, 255f, 130f, 255f);
                //sample = Color.red;

                break;
            case 2:
                sample = ConVertColor(255f, 210f, 130f, 255f);
                //sample = Color.blue;

                break;
            case 3:
                
                sample = ConVertColor(255f, 130f, 170f, 255f);
                //sample = ConVertColor(200f, 150f, 16f, 255f);//new Color((float)200/255, (float)150 /255, (float)16 /255,1f);

                break;
            case 4:
                sample = ConVertColor(130f, 255f, 130f, 255f);
                //sample = ConVertColor(150f, 0f, 255f, 255f);//new Color(150,0,255,255);
                break;
            case 5:
                sample = ConVertColor(255f, 190f, 130f, 255f);
                //sample = ConVertColor(255f, 255f, 0f, 255f);// new Color(255,255,0,255);

                break;
            case 6:
                sample = ConVertColor(255f, 130f, 200f, 255f);
                //sample = ConVertColor(255f, 200f, 225f, 255f); //new Color(255, 200, 225, 255);

                break;
            case 7:
                sample = ConVertColor(130f, 250f, 255f, 255f);
                //sample = ConVertColor(150f, 255f, 225f, 255f); //new Color(150, 255, 225, 255);
             
                break;
            case 8:
                sample = ConVertColor(255f, 160f, 130f, 255f);
                //sample = Color.black;

                break;
        }
        //
        foreach (MeshRenderer mesh in m_listRenderer)
            mesh.material.color = sample;
 
     
    }
    private Color ConVertColor(float x, float y, float z, float v)
    {
        x = x / 255;
        y = y / 255;
        z = z / 255;
        v = v / 255;
        return new Color(x, y, z, v);
    }
    private void ChangeText(int value)
    {
        int count = 0;
        int index = value;
        while (index % 10 > 0)
        {
            count++;
            index = index / 10;
        }
        //Debug.Log(count + "==================");
        switch (count)
        {
            case 1:
            case 2:
            case 3:
                m_sizeTextValue = 1.93f;
                break;
            case 4:
                m_sizeTextValue = 1.16f;

                break;
            case 5:
                m_sizeTextValue = 1.26f;

                break;
            case 6:
                m_sizeTextValue = 1.08f;

                break;
        }
        m_textValue.text = value.ToString();
        m_textValue.fontSize = m_sizeTextValue;
     
    }
    private void OnTrigger()
    {
        StartCoroutine(WaitOnTrigger());
    }
    IEnumerator WaitOnTrigger()
    {
        yield return new WaitForSeconds(1f);
        GameController.OnCompleteLevel();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (m_isTrigger)
            return;
        if (collision.gameObject.tag == "cube")
        {
            // m_planeChild.SetActive(false);
            OnTrigger();
        }

    }
    private void OnTriggerEnter(Collider collider)
    {
        if (!m_isTrigger)
            return;
        if(collider.tag == "cube")
        {
             m_planeChild.SetActive(false);
             m_planeRoll.SetActive(true);
        }
    }
}
