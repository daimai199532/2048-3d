using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody m_rigidbody;
    [SerializeField] private Transform m_cubeRight;
    [SerializeField] private Transform m_cubeLeft;
    [SerializeField] private Animator m_cubeCenter;
    [SerializeField] private float m_limitValue;
    [SerializeField] private List<TextMeshProUGUI> m_listTextValues;
    [SerializeField] private List<TextMeshProUGUI> m_listChildTextValues;
    [SerializeField] private SkinnedMeshRenderer m_rendererCenter;
    [SerializeField] private List<SkinnedMeshRenderer> m_listChildRenderers;
    [SerializeField] private List<TrailRenderer> m_listTrailRenderers;
    [SerializeField] private int m_value;
    [SerializeField] private int m_color;
    [SerializeField] private Transform m_playerCenter;

    private float m_sizeTextValue = 0;
    // Update is called once per frame
    private bool m_isMaxAnim = false;
    //
    private Animator m_animCubeLeft;
    private Animator m_animCubeRight;
    private Vector3 m_startPosLeft; // position start by m_left
    private Vector3 m_startPosRight; // position start by m_right
    private bool m_isMoveLeft = false;
    private bool m_isDoubleCube = false;
    private bool m_isControll = false;
    private bool m_isCameraFollow = true;
    private void Awake()
    {
        GameController.pauseGameEvent += OnPauseGame;
        //
        m_startPosLeft = m_cubeLeft.transform.localPosition;
        m_startPosRight = m_cubeRight.transform.localPosition;
        m_animCubeLeft = m_cubeLeft.GetComponent<Animator>();
        m_animCubeRight = m_cubeRight.GetComponent<Animator>();
        //
        OnSetDefault();
        //m_cubeCenter.SetBool("isFinish", true);
    }
    private void OnDestroy()
    {
        GameController.pauseGameEvent -= OnPauseGame;
    }
    void Update()
    {
        if (!m_isControll)
            return;
        if (Input.GetMouseButtonDown(0))
        {
            OnAnimMax();
        }
        
    }
    private void LateUpdate()
    {
        if(m_isCameraFollow)
        {
            GameController.UpdateCamera(transform.position, transform.eulerAngles, true);
        }
       
    }
    private void OnPauseGame(bool isPause, bool isGameOver)
    {
        m_isControll = !isPause;
    }
    public void OnStepGameComplete()
    {
        m_isCameraFollow = false;
        //
        foreach (TextMeshProUGUI text in m_listTextValues)
        {
            text.gameObject.SetActive(false);
        }
        //
        GameController.OnStepCompleteLevel(m_cubeCenter.gameObject);
    }
    public void OnJumpPlayerCenter() // Jump ne chuong ngai vat va dien anim
    {

        m_isControll = false;
        //m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        m_rigidbody.constraints = RigidbodyConstraints.FreezePosition;
        m_rigidbody.useGravity = false;
        //m_playerCenter.transform.position = Vector3.zero;
        m_cubeCenter.SetBool("isSalto", true);
        Sequence seq = DOTween.Sequence();
        seq.Append(m_playerCenter.DOMoveY(m_playerCenter.position.y + 1f, 1f));
        seq.AppendInterval(1f);
        seq.OnComplete(() =>
        {
            //m_rigidbody.transform.position = new Vector3(0, m_rigidbody.transform.position.y, m_rigidbody.transform.position.z);
            m_rigidbody.constraints = RigidbodyConstraints.FreezePositionX  | RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;
            m_rigidbody.useGravity = true;
            m_cubeCenter.SetBool("isSalto",false);
            Debug.Log("Jump complete ===============");
            m_isControll = true;
            //Debug.Log("On Complete");
        });
        //seq.Append(m_playerCenter.DOMoveY(0, 1f));
    }
  
    public void OnHurt()
    {
        SoundManager.PlaySound("Hit", false, true);
        m_value %= 2;
        m_color--;
        if (m_value < 4)
            OnDied();
        else
        {
            OnSetDefault();
            GameController.UpdateObjectGameOver(m_color);
        }

    }
    public void OnDied()
    {
        if (!m_isControll)
            return;
        m_isControll = false;
        m_playerCenter.DOMoveY(m_playerCenter.position.y - 1f, 0.5f).OnComplete(GameController.UpdateOpenPanelGameOver);
        //Debug.Log("Game Over");
        //StartCoroutine(WaitOnDied());
    }
    IEnumerator WaitOnDied()
    {
        yield return new WaitForSeconds(0.5f);
        GameController.OnGameOver();
    }
    public void OnUpdateLevel(bool isUpLevel) // on anim + change text-color +
    {
        if(isUpLevel)
        {
            m_value *= 2;
            m_color++;
        }
        else
        {
            m_value /= 2;
            m_color--;
        }
        OnSetDefault();
        OnEat();
        GameController.UpdateObjectGameOver(m_color);
    }
    private void OnSetDefault()
    {
        ChangeColor(m_color);
        ChangeText(m_value);
    }
    private void ChangeColor(int color)
    {
        Color sample = Color.black;
        Color childSample = Color.green;
        int tempColor = color % 9;
        switch (tempColor)
        {
            case 0:
                sample = Color.green;
                childSample = ConVertColor(150f, 135f, 225f, 255f);
                break;
            case 1:
                sample = Color.red;
                childSample = Color.green;
                break;
            case 2:
                sample = Color.blue;
                childSample = Color.red;
                break;
            case 3:
                sample = ConVertColor(200f, 150f, 16f, 255f);//new Color((float)200/255, (float)150 /255, (float)16 /255,1f);
                childSample = Color.blue;
                break;
            case 4:
                sample = ConVertColor(150f, 0f, 255f, 255f);//new Color(150,0,255,255);
                childSample = ConVertColor(200f, 150f, 16f, 255f);
                break;
            case 5:
                sample = ConVertColor(255f, 255f, 0f, 255f);// new Color(255,255,0,255);
                childSample = ConVertColor(150f, 0f, 255f, 255f);
                break;
            case 6:
                sample = ConVertColor(255f, 200f, 225f, 255f); //new Color(255, 200, 225, 255);
                childSample = ConVertColor(255f, 255f, 0f, 255f);
                break;
            case 7:
                sample = ConVertColor(150f, 255f, 225f, 255f); //new Color(150, 255, 225, 255);
                childSample = ConVertColor(255f, 200f, 225f, 255f);
                break;
            case 8:
                sample = ConVertColor(150f, 135f, 225f, 255f);
                childSample = ConVertColor(150f, 255f, 225f, 255f);
                break;
        }
        //
        m_rendererCenter.material.color = sample;
        foreach (SkinnedMeshRenderer mesh in m_listChildRenderers)
            mesh.material.color = childSample;
        //
        m_listTrailRenderers[0].startColor = sample;
        for (int i = 1; i < m_listTrailRenderers.Count; i++)
            m_listTrailRenderers[i].startColor = childSample;

    }
    private Color ConVertColor(float x, float y, float z, float v)
    {
        x = x / 255;
        y = y / 255;
        z = z / 255;
        v = v / 255;
        return new Color(x, y, z, v);
    }
    private void ChangeText(int value)
    {
        float sizeChildText = 0.0058f;
        int count = 0;
        int index = value;
        while (index % 10 > 0)
        {
            count++;
            index = index / 9;
        }
        //Debug.Log(count + "==================" + value);
        switch (count)
        {
            case 1:
                m_sizeTextValue = 0.0058f;
                break;
            case 2:
                m_sizeTextValue = 0.0058f;
                break;
            case 3:
                m_sizeTextValue = 0.0058f;
                break;
            case 4:
                m_sizeTextValue = 0.0044f;
                sizeChildText = 0.0058f;
                break;
            case 5:
                m_sizeTextValue = 0.0033f;
                sizeChildText = 0.0044f;
                break;
        }
        //
        foreach (TextMeshProUGUI text in m_listTextValues)
        {
            text.text = value.ToString();
            text.fontSize = m_sizeTextValue;
        }
        //
        foreach (TextMeshProUGUI text in m_listChildTextValues)
        {
            text.text = (value/2).ToString();
            text.fontSize = sizeChildText;
        }
    }

    public int GetValueIndex()
    {
        return m_value;
    }
    public bool GetIsDoubleCube()
    {
        return m_isDoubleCube;
    }
    
    private void OnEat()
    {

        //if (m_isMoveLeft)
        //{
        //    m_animCubeLeft.SetBool("isEat", true);
        //    m_animCubeRight.SetBool("isEat", true);

        //}
        //else
        //{
        //    m_cubeCenter.SetBool("isEat", true);
        //    StartCoroutine(WaitCompleteEat());
        //    //Debug.Log(m_cubeCenter.runtimeAnimatorController.animationClips);
        //}
        StartCoroutine(WaitCompleteEat());
    }
    private IEnumerator WaitCompleteEat()
    {
        yield return new WaitForSeconds(0.01f);
        SoundManager.PlaySound("Sound_merge", false, true);
        //m_cubeCenter.SetBool("isEat", false);
        //
        if (m_isMoveLeft)
        {
            m_animCubeLeft.SetBool("isEat", false);
            m_animCubeRight.SetBool("isEat", false);
            yield return new WaitForSeconds(0.01f);
            m_animCubeLeft.SetBool("isEat", true);
            m_animCubeRight.SetBool("isEat", true);

        }
        else
        {
            m_cubeCenter.SetBool("isEat", false);
            yield return new WaitForSeconds(0.01f);
            m_cubeCenter.SetBool("isEat", true);
           
            //Debug.Log(m_cubeCenter.runtimeAnimatorController.animationClips);
        }
    }
    private void OnAnimMax()
    {
        //on left/right -> off center -> move left/right 

        m_isMaxAnim = !m_isMaxAnim;
        if(m_isMaxAnim)
        {
            MoveToLeft();
            //Debug.Log("move left");
        }
        else
        {
            MoveToCenter();
            //Debug.Log("move center");
        }
     
    }
    public void BntOnClick()
    {
        //OnAnimMax();
    }
    public void OnDefaultMoveCenter()
    {
        m_isControll = false;
        if (m_isDoubleCube)
            MoveToCenter();
    }
    private void MoveToLeft()
    {
        m_isDoubleCube = true;
        if (m_isMoveLeft)
            return;
        m_isMoveLeft = true;
        float posCurrent = m_cubeLeft.transform.localPosition.x;
        float duration = 0.75f;
        float distance = -1.1f;
        float speed = (m_startPosLeft.x + distance) / duration;

        //
        float quangDuongCanDi = (m_startPosLeft.x + distance) - posCurrent;
        float timeCanDi = quangDuongCanDi / speed;
        //
        m_cubeCenter.gameObject.SetActive(false);
        m_cubeLeft.gameObject.SetActive(true);
        m_cubeRight.gameObject.SetActive(true);
        m_animCubeLeft.SetBool("isLeft", true);
        m_animCubeRight.SetBool("isLeft", true);
        //m_animCubeRight.SetBool("isRight", true);
        //
        m_cubeLeft.DOKill();
        m_cubeLeft.DOLocalMoveX((m_startPosLeft.x + distance), timeCanDi).OnComplete(() => {
            m_animCubeLeft.SetBool("isLeft", false);
        });
        m_cubeRight.DOKill();
        m_cubeRight.DOLocalMoveX((m_startPosRight.x + distance *(-1)), timeCanDi).OnComplete(()=>{
            m_animCubeRight.SetBool("isLeft", false);
        });
    }
    private void MoveToCenter()
    {
        if (!m_isMoveLeft)
            return;
        m_isMoveLeft = false;
        float posCurrent = m_cubeLeft.transform.localPosition.x;
        float duration = 0.75f;
        float distance = -1.1f;
        float speed = (m_startPosLeft.x + distance) / duration;

        //
        float quangDuongCanDi = posCurrent;
        float timeCanDi = quangDuongCanDi / speed;
        //
        //StartCoroutine(OnObjectCuve(false, timeCanDi - 0.1f));
        //Debug.Log(m_startPosLeft.x + "===time center ===========" + timeCanDi);
        //
        m_animCubeLeft.SetBool("isLeft", false);
        m_animCubeRight.SetBool("isLeft", false);
        //m_animCubeRight.SetBool("isRight", false);
        //
        m_cubeLeft.DOKill();
        m_cubeLeft.DOLocalMoveX(m_startPosLeft.x, timeCanDi);
        m_cubeRight.DOKill();
        m_cubeRight.DOLocalMoveX(m_startPosRight.x, timeCanDi).OnComplete(()=>{
            m_cubeCenter.gameObject.SetActive(true);
            m_cubeLeft.gameObject.SetActive(false);
            m_cubeRight.gameObject.SetActive(false);
            m_isDoubleCube = false;
        });
    }
  
}
