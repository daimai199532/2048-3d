using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MiniGamePlayScene : MonoBehaviour
{
    [SerializeField] private GameObject m_panelTutorial;
    [SerializeField] private GameObject m_panelGameOVer;
    [SerializeField] private GameObject m_panelGameComplete;
    private void Awake()
    {
        GameController.updateOpenPanelGameOverEvent += OpenPanelGameOver;
        GameController.onCompleteLevelEvent += OpenPanelGameComplete;
        //
        SoundManager.PlaySound("Ambient", false,true);
    }
    private void OnDestroy()
    {
        GameController.updateOpenPanelGameOverEvent -= OpenPanelGameOver;
        GameController.onCompleteLevelEvent -= OpenPanelGameComplete;
    }
    private void OpenPanelGameOver()
    {
        m_panelGameOVer.SetActive(true);
    }
    private void OpenPanelGameComplete()
    {
        m_panelGameComplete.SetActive(true);
    }
    public void HomeOnClick()
    {
        SceneManager.LoadScene(0);
        
    }
    public void RestartOnClick()
    {
        SceneManager.LoadScene(1);
    }
    public void TutorialOnClick()
    {
        m_panelTutorial.SetActive(false);
        GameController.PauseGame(false, false);
    }
    IEnumerator WaitTutorial()
    {
        yield return new WaitForSeconds(0.5f);
        GameController.PauseGame(false, false);
    }
}
