using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectEndControll : MonoBehaviour
{
    private bool m_isTrigger = false;
    private void OnTriggerExit(Collider collider)
    {
        if (m_isTrigger)
            return;
        if (collider.tag == "Player")
        {
            m_isTrigger = true;
            PlayerMovement player = collider.gameObject.transform.parent.parent.GetComponent<PlayerMovement>();
            //Debug.Log("lock player" + collider.gameObject.transform.parent.parent);
            if (player == null)
                return;
            
            player.OnDefaultMoveCenter();
        }
    }
}
