using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ObjectBox : MonoBehaviour
{
    [SerializeField] private int m_value;
    [SerializeField] private int m_color;
    [SerializeField] private List<SkinnedMeshRenderer> m_listRenderers;
    [SerializeField] private List<TextMeshProUGUI> m_listTextValues;
    [SerializeField] private ObjectBox m_box;
    private float m_sizeTextValue = 0 ;

    private void Awake()
    {
        ChangeColor(m_color);
        ChangeText(m_value);
    }
    // Start is called before the first frame update
    private void ChangeColor(int color)
    {
        Color sample = Color.green;
        switch (color)
        {
            case 0:
                sample = Color.green;
           
                break;
            case 1:
                sample = Color.red;
                
                break;
            case 2:
                sample = Color.blue;
                
                break;
            case 3:
                sample = ConVertColor(200f, 150f, 16f, 255f);//new Color((float)200/255, (float)150 /255, (float)16 /255,1f);
                 break;
            case 4:
                sample = ConVertColor(150f, 0f, 255f, 255f);//new Color(150,0,255,255);
                 break;
            case 5:
                sample = ConVertColor(255f, 255f, 0f, 255f);// new Color(255,255,0,255);
                break;
            case 6:
                sample = ConVertColor(255f, 200f, 225f, 255f); //new Color(255, 200, 225, 255);
                break;
            case 7:
                sample = ConVertColor(150f, 255f, 225f, 255f); //new Color(150, 255, 225, 255);
                break;
            case 8:
                sample = ConVertColor(150f, 135f, 225f, 255f);
                break;
        }
        //
        foreach(SkinnedMeshRenderer mesh in m_listRenderers)
                mesh.material.color = sample;
    }
    private Color ConVertColor(float x, float y, float z, float v)
    {
        x = x / 255;
        y = y / 255;
        z = z / 255;
        v = v / 255;
        return new Color(x, y, z, v);
    }
    private void ChangeText(int value)
    {

        int count = 0;
        int index = value;
        while (index % 10 > 0)
        {
            count++;
            index = index / 10;
        }
        //Debug.Log(count + "==================");
        switch (count)
        {
            case 1:
                m_sizeTextValue = 0.0058f;
                break;
            case 2:
                m_sizeTextValue = 0.0058f;
                break;
            case 3:
                m_sizeTextValue = 0.0058f;
                break;
            case 4:
                m_sizeTextValue = 0.0044f;
                break;
            case 5:
                m_sizeTextValue = 0.0033f;
                break;
        }
        //
        foreach (TextMeshProUGUI text in m_listTextValues)
        {
            text.text = value.ToString();
            text.fontSize = m_sizeTextValue;
        }
        //

    }
    public int GetValue()
    {
        return m_value;
    }
    public float GetPositionZ()
    {
        return transform.position.z;
    }
    public void UpdateLevel(ObjectBox box)
    {
        if (box != m_box)
            return;
        m_color++;
        m_value *= 2;
        ChangeColor(m_color);
        ChangeText(m_value);
    }
    public void OnDestroyGameObject(ObjectBox box)
    {
        if (box != m_box)
            return;
        Destroy(gameObject, 0.1f);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            PlayerMovement player = collision.transform.parent.GetComponent<PlayerMovement>();
           
            if (player == null)
                return;
            //Debug.Log(m_value + "==========" + player.GetIsDoubleCube());
            if (player.GetIsDoubleCube())
            {
                if (m_value == player.GetValueIndex()/2) // Kiem tra GetValue &&  single hay la double
                {
                    player.OnUpdateLevel(true);
                    Destroy(gameObject);
                }
            }else
            {
                if (m_value == player.GetValueIndex()) // Kiem tra GetValue &&  single hay la double
                {
                    player.OnUpdateLevel(true);
                    Destroy(gameObject);
                }
            }
           
        }
        if(collision.gameObject.tag == "cube")
        {
            ObjectBox box = collision.gameObject.GetComponent<ObjectBox>();
            if (box == null)
                return;
            if(box.GetValue() == m_value)
            {
                if (box.GetPositionZ() > transform.position.z)
                {
                    box.UpdateLevel(box);
                    OnDestroyGameObject(m_box);
                }
                else
                {
                    UpdateLevel(m_box);
                    OnDestroyGameObject(box);
                }

            }
        }
    }
}
