using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
public class SpawnCube : MonoBehaviour
{
    [SerializeField] private GameObject m_colliderGameOver;
    [SerializeField] private ObjectHeart m_prefabObjHeart;
    [SerializeField] private ObjectBoom m_prefabObjBoom;
    [SerializeField] private List<CubeMove> m_prefabCubes;

    private float m_timeRestart;
    private bool m_isSpawnCube = false;
    private GameObject m_currentCube;
    //private List<int> m_listRandoms = new List<int>();
    private int m_currentRandom =-1;
    private bool m_isPause = false;
    private void Awake()
    {
        GameController.pauseGameEvent += OnPause;
        GameController.createObjBoomEvent += CreateObjBoom;
        GameController.createObjHeartEvent += CreateObjHeart;
    }
    private void OnDestroy()
    {
        GameController.pauseGameEvent -= OnPause;
        GameController.createObjBoomEvent -= CreateObjBoom;
        GameController.createObjHeartEvent -= CreateObjHeart;
    }
  
    private void Start()
    {
        CreateCube();
        //
        SoundManager.PlaySound("casual-05", true);
    }
    private void Update()
    {
      
        if (m_isPause)
            return;
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {

            m_colliderGameOver.SetActive(false);
            CubeMove.moveTop?.Invoke();
            m_timeRestart = 0;
            //
            m_isSpawnCube = true;
            SoundManager.PlaySound("Pop (6)",false);
        }

    }
   
    private void FixedUpdate()
    {
        if (m_isPause)
            return;
        //if (m_currentCube == null)
        //    return;
        float x = Input.GetAxisRaw("Horizontal");
        CubeMove.move?.Invoke(x);

        if (m_isSpawnCube)
        {
            m_timeRestart += Time.deltaTime;
            
            if (m_timeRestart > 0.3f)
            {
                CreateCube();
            }
           
        }

    }

    // Create -> controll move -> 
    private void CreateCube()
    {
      
        m_isSpawnCube = false;

        int count = m_prefabCubes.Count;
        int rand = Random.RandomRange(0, count);
        if (m_currentRandom < 0)
        {
            rand = 0;
            m_currentRandom = 0;
        }
        else
        {
            while (rand == m_currentRandom)
            {
                rand = Random.RandomRange(0, count);
            }
            m_currentRandom = rand;
        }
        CubeMove cube = Instantiate(m_prefabCubes[rand]);

        m_currentCube = cube.gameObject;
        cube.transform.position = transform.position;
        cube.gameObject.SetActive(true);
        cube.Init();
        //
        m_colliderGameOver.SetActive(true);
        
    }
    private void CreateObjBoom()
    {
        Destroy(m_currentCube);

        m_isSpawnCube = false;

        ObjectBoom obj = Instantiate(m_prefabObjBoom);

        m_currentCube = obj.gameObject;
        obj.transform.position = transform.position;
        obj.gameObject.SetActive(true);
        obj.Init();
    }
    private void CreateObjHeart()
    {
        Destroy(m_currentCube);

        m_isSpawnCube = false;

        ObjectHeart obj = Instantiate(m_prefabObjHeart);

        m_currentCube = obj.gameObject;
        obj.transform.position = transform.position;
        obj.gameObject.SetActive(true);
        obj.Init();
    }
    private void OnPause(bool pause,bool isGameOver)
    {
       m_isPause = pause;
    }
   
}
