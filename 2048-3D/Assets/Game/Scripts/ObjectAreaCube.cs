using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectAreaCube : MonoBehaviour
{
    [SerializeField] private Collider m_collider;

    IEnumerator WaitOnCompleteTrigger()
    {
        m_collider.enabled = true;
        yield return new WaitForSeconds(1f);
        //m_isTrigger = false;
        m_collider.enabled = false;
        yield return new WaitForSeconds(0.5f);
        GameController.PauseGame(false,false);
    }
    public void NextGameOnClick()
    {
        StartCoroutine(WaitOnCompleteTrigger());
    }
    private void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "cube")
        {
            
            Cube cube = collider.GetComponent<Cube>();
            if (cube == null)
                return;
            GameController.AddNewObjectCube(cube, false, false);
        }
    }
}
