using System.Collections;
using System;
using UnityEngine;

public  class GameController 
{
    // Event Object GamePlay
    public static Action createObjHeartEvent;
    public static Action createObjBoomEvent;
    public static Action<Cube> destroyObjectCubeEvent;
    public static Action<Cube, Vector3?, GameObject> updateLevelObjectCubeEvent;
    public static Action<Cube,bool, bool> addNewObjectCubeEvent;
    public static Action<Vector3,Vector3,bool> updateCameraEvent;
    public static Action<GameObject> onStepCompleteLevelEvent;
    public static Action onCompleteLevelEvent;
    public static Action onGameOverEvent;
    public static Action<int> updateObjectFinish;
    // Event UI
    public static Action<bool,bool> pauseGameEvent;
    public static Action<int, int > updateOpenPanelRewardCubeEvent;
    public static Action updateOpenPanelRewardHeart;
    public static Action updateOpenPanelGameOverEvent;
    public static Action<Vector3, int> updateTextCubeEvent;
    //
    public static Action<int> updateScoreEvent;
    public static Action<int> updateBoomEvent;

    public static void DestroyObjectCube(Cube cube)
    {
        destroyObjectCubeEvent?.Invoke(cube);
        //AddNewObjectCube(cube, false, false);
    }
    public static void UpdateLevelObjectCube(Cube cube, Vector3? target = null, GameObject? obj = null)
    {
     
        updateLevelObjectCubeEvent?.Invoke(cube, target, obj);
    }
    //
    public static void UpdateOpenPanelRewardCube(int value, int color)
    {
        updateOpenPanelRewardCubeEvent?.Invoke(value, color);
    }
    public static void UpdateOpenPanelRewardHeart()
    {
        updateOpenPanelRewardHeart?.Invoke();
    }
    public static void UpdateOpenPanelGameOver()
    {
        //Debug.Log( "=================================");
        updateOpenPanelGameOverEvent?.Invoke();
    }
    public static void UpdateTextCube(Vector3 pos, int value)
    {
        updateTextCubeEvent?.Invoke(pos, value);
    }
    public static void UpdateBoom(int value)
    {
        MainModel.UpdateBoom(value);
        updateBoomEvent?.Invoke(value);
    }
    public static void UpdateScore(int value)
    {
        MainModel.UpdateScore(value);
        updateScoreEvent?.Invoke(value);
    }
    public static void PauseGame(bool pause, bool isGameOver) // change physic, stop action
    {
        pauseGameEvent?.Invoke(pause, isGameOver);
        if(isGameOver)
            ChangeStatusGame(status.gameOver);
        else
        {
            if (!pause)
                ChangeStatusGame(status.play);
            else
                ChangeStatusGame(status.stop);
        }
      
    }
    public static void CreateObjHeart()
    {
        createObjHeartEvent?.Invoke();
    }
    public static void CreateObjBoom()
    {
        createObjBoomEvent?.Invoke();
    }
    public static void AddNewObjectCube(Cube cube, bool isAdd, bool isFirst)
    {
        addNewObjectCubeEvent?.Invoke(cube, isAdd, isFirst);
    }

    public static void UpdateCamera(Vector3 pos, Vector3 rotate, bool isPlay)
    {
        updateCameraEvent?.Invoke(pos, rotate, isPlay);
    }
    public static void OnStepCompleteLevel(GameObject obj)
    {
        onStepCompleteLevelEvent?.Invoke(obj);
    }
    public static void OnCompleteLevel()
    {
        onCompleteLevelEvent?.Invoke();
    }
    public static void OnGameOver()
    {
        onGameOverEvent?.Invoke();
    }
    public static void ChangeStatusGame(status valueStatus)
    {
        MainModel.ChangeStatusGame(valueStatus);
    }
    public static void UpdateObjectGameOver(int color)
    {
        updateObjectFinish?.Invoke(color);
    }
}
