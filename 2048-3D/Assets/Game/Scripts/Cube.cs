using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class Cube : MonoBehaviour
{
   
    [SerializeField] private  List<TextMeshProUGUI> m_listTextValues;
    [SerializeField] private Rigidbody m_rigidbody;
    [SerializeField] private MeshRenderer m_renderer;
    [SerializeField] private int m_numberColor;
    [SerializeField] private int m_value;
    [SerializeField] private ParticleSystem m_effectLevelUp;
    //[SerializeField] private float m_priority;
    [SerializeField] private Cube m_cube;
    [SerializeField] private CubeMove m_cubeMove;
    public GameObject targetObj;
    private bool m_isMerger = true;
    private float m_sizeTextValue = 0;
    //private int m_gameOverCount = 0;
    private void OnEnable()
    {
        GameController.destroyObjectCubeEvent += DestroyGameObject;
        GameController.updateLevelObjectCubeEvent += UpdateLevel;
        GameController.pauseGameEvent += OnPause;
        //gameObject.tag = "Cube";
    }
    private void OnDestroy()
    {
        GameController.pauseGameEvent -= OnPause;
    }
    private void DestroyGameObject(Cube cube)
    {
        if(cube == m_cube)
        {
            //GameController.AddNewObjectCube(cube, false, false);
            Destroy(gameObject);
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
       
        InitProperties();
    }
   
    private void InitProperties()
    {
        ChangeColor(m_numberColor);
        ChangeText(m_value);
        if(!m_cubeMove.GetControll())
            GameController.AddNewObjectCube(m_cube, true, true);
    }
    private void ChangeColor(int color)
    {
        switch(color)
        {
            case 0:
                m_renderer.material.color = Color.green;
                break;
            case 1:
                m_renderer.material.color = Color.red;
                break;
            case 2:
                m_renderer.material.color = Color.blue;
                break;
            case 3:
                m_renderer.material.color = ConVertColor(200f, 150f, 16f, 255f);//new Color((float)200/255, (float)150 /255, (float)16 /255,1f);
                break;
            case 4:
                m_renderer.material.color = ConVertColor(150f, 0f, 255f, 255f);//new Color(150,0,255,255);
                break;
            case 5:
                m_renderer.material.color = ConVertColor(255f, 255f, 0f, 255f);// new Color(255,255,0,255);
                break;
            case 6:
                m_renderer.material.color = ConVertColor(255f, 200f, 225f, 255f); //new Color(255, 200, 225, 255);
                break;
            case 7:
                m_renderer.material.color = ConVertColor(150f, 255f, 225f, 255f); //new Color(150, 255, 225, 255);
                break;
            case 8:
                m_renderer.material.color = Color.black;
                break;
        }
    }
    private Color ConVertColor(float x, float y, float z, float v)
    {
        x = x / 255;
        y = y / 255;
        z = z / 255;
        v = v / 255;
        return new Color(x, y, z, v);
    }
    private void ChangeText(int value)
    {
       
        int count = 0;
        int index = value;
        while (index % 10 > 0)
        {
            count++;
            index = index / 10;
        }
        //Debug.Log(count + "==================");
        switch(count)
        {
            case 1:
                m_sizeTextValue = 0.58f;
                break;
            case 2:
                m_sizeTextValue = 0.58f;
                break;
            case 3:
                m_sizeTextValue = 0.58f;
                break;
            case 4:
                m_sizeTextValue = 0.44f;
                break;
            case 5:
                m_sizeTextValue = 0.33f;
                break;
        }
        //
        foreach (TextMeshProUGUI text in m_listTextValues)
        {
            text.text = value.ToString();
            text.fontSize = m_sizeTextValue;
        }
        //

    }
    private void OnPause(bool pause,bool isGameOver)
    {
        //Debug.Log("==============" + pause);
        if (pause)
            m_rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        else
            m_rigidbody.constraints = RigidbodyConstraints.None;
            //    m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }
    public float GetPriority()
    {
        return transform.position.z;
    }
    public int GetValue()
    {
        return m_value;
    }
    public bool GetControll()
    {
        return m_cubeMove.GetControll();
    }
    private void UpdateLevel(Cube cube, Vector3? target, GameObject obj) // change color + afforce + change text + add effect 
    {
        if (cube != m_cube)
            return;
        targetObj = obj;
        SoundManager.PlaySound("collect_item_15", false);
        Vector3 vector = transform.position;
        m_rigidbody.velocity = Vector3.zero;
        //StartCoroutine(WaitColliderTop());
        // 
        Vector3 direction;
        float distance;
        m_rigidbody.AddForce(Vector2.up * 550f);
        if (target != null)
        {
            direction = target.Value - transform.position;
            distance = Vector3.Distance(target.Value, transform.position);
               // m_rigidbody.AddForce(direction*distance * 10f);
            m_rigidbody.AddForce(new Vector3(direction.x * distance * 12, 0, direction.z * distance * 12));

        }
        //
        m_numberColor++; 
        ChangeColor(m_numberColor);
        //
        m_value *= 2;
        ChangeText(m_value);
        //
        //GameController.AddNewObjectCube(cube, true, false);
        GameController.UpdateScore(m_value);
        if(m_value > 128)
        {
            StartCoroutine(WaitPause());    
        }
        GameController.UpdateTextCube(transform.position, m_value);
        StartCoroutine(WaitOnEffect());
        m_rigidbody.constraints = RigidbodyConstraints.None;
    }
    IEnumerator WaitPause()
    {
        
        yield return new WaitForSeconds(0.5f);
        GameController.PauseGame(true,false);
        yield return new WaitForSeconds(0.5f);
        GameController.UpdateOpenPanelRewardCube(m_value, m_numberColor);
    } 
    IEnumerator WaitLateMerger()
    {
        m_isMerger = false;
        yield return new WaitForSeconds(0.1f);
        m_isMerger = true;
    }
    IEnumerator WaitColliderGround()
    {
        //yield return new WaitForSeconds(0.1f);
        //m_rigidbody.constraints = RigidbodyConstraints.FreezePositionZ;
        ////m_rigidbody.mass = 5;
        yield return new WaitForSeconds(0.5f);
        //m_rigidbody.mass = 1;
        m_rigidbody.constraints = RigidbodyConstraints.None;
    }
    IEnumerator WaitOnEffect()
    {
        m_effectLevelUp.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        m_effectLevelUp.Play();
        //m_rigidbody.mass = 5;
        yield return new WaitForSeconds(1.5f);
        //m_rigidbody.mass = 1;
        m_effectLevelUp.Pause();
        m_effectLevelUp.gameObject.SetActive(false);
    }
    private void OnCollisionEnter(Collision collision)
    {
       
        if(collision.gameObject.tag == "ground" && m_rigidbody.velocity == Vector3.zero)//transform.rotation.y < 0.1 && transform.rotation.y > -0.1f)
        {
            m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        }
        if(collision.gameObject.tag == "cube")
        {
            
            Cube cube = collision.gameObject.GetComponent<Cube>();
            if (cube == null)
                return;
            float priority = cube.GetPriority();
            int value = cube.GetValue();

            if (priority > transform.position.z &&  value == m_value  && m_isMerger) // check do uu tien + value 
            {

                //GameController.DestroyObjectCube(cube);
                //
                GameController.AddNewObjectCube(cube, false, false);
                GameController.AddNewObjectCube(m_cube,true, false);
                //
                StartCoroutine(WaitLateMerger());
            }
        }
        if (collision.gameObject.tag == "gameOver")
        {
           
            GameController.PauseGame(true, true);
            GameController.UpdateOpenPanelGameOver();
        } 
       

    }
}
