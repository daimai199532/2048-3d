using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private static SoundManager s_api;
    [SerializeField] private GameObject m_audioPrefab;
    [SerializeField] private AudioClip[] m_clips;

    private AudioSource m_music;
    private List<AudioSource> m_sounds;
    private bool m_musicOn;
    private bool m_soundOn;
    //
    private Dictionary<string, float> m_durationConfig;
    private Dictionary<string, float> m_duration;
    private void Awake()
    {
        if (s_api == null)
            s_api = this;

        LoadCache();
        m_durationConfig = new Dictionary<string, float>(){
            {"", 0.1f}
        };
        m_duration = new Dictionary<string, float>();
    }
    public static bool IsMusicOn()
    {
        return s_api.m_musicOn;
    }
    public static bool IsSoundOn()
    {
        return s_api.m_soundOn;
    }
    public static void MuteSound(bool mute, bool save = true)
    {
        if(save)
        {
            s_api.m_musicOn = !mute;
            s_api.SaveCache();
        }
        else
        {
            if (mute)
                s_api.m_soundOn = false;
            else
                s_api.SaveCache();
        }
        //
        List<AudioSource> newArr = new List<AudioSource>();
        foreach (AudioSource s in s_api.m_sounds)
        {
            if (s == null)
                continue;
            newArr.Add(s);
            s.volume = s_api.m_soundOn ? 1 : 0;
        }
        s_api.m_sounds = newArr;
    }
    public static void MuteMusic(bool mute, bool save = true)
    {
        if (save)
        {
            s_api.m_musicOn = !mute;
            s_api.SaveCache();
        }
        else
        {
            if (mute)
                s_api.m_musicOn = false;
            else
                s_api.LoadCache();
        }
        if (s_api.m_music == null)
            return;
        s_api.m_music.volume = s_api.m_musicOn ? 1 : 0;
    }
    private void SaveCache()
    {
        PlayerPrefs.SetInt("PLAYER_PREF_MUSIC", m_musicOn ? 1 : 0);
        PlayerPrefs.SetInt("PLAYER_PREF_SOUND", m_soundOn ? 1 : 0);
        PlayerPrefs.Save();
    }
    private void LoadCache()
    {
        m_musicOn = PlayerPrefs.GetInt("PLAYER_PREF_MUSIC", 1) == 1;
        m_soundOn = PlayerPrefs.GetInt("PLAYER_PREF_SOUND", 1) == 1;
    }
    public static GameObject PlaySound(string clipName, bool loop, bool music = false)
    {
        if (music)
        {
            if (s_api.m_music == null)
            {
                GameObject go = Instantiate(s_api.m_audioPrefab);
                AudioHelper source = go.GetComponent<AudioHelper>();
                source.Play(s_api.GetClip(clipName), loop);
                s_api.m_music = go.GetComponent<AudioSource>();
                s_api.m_music.volume = s_api.m_musicOn ? 1 : 0;
            }
            return s_api.m_music.gameObject;
        }
        else
        {
            if (!loop)
            {
                if (s_api.m_durationConfig.ContainsKey(clipName) && s_api.m_duration.ContainsKey(clipName))
                {
                    float delTime = Time.time - s_api.m_duration[clipName];
                    if (delTime < s_api.m_durationConfig[clipName])
                        return null;
                }
            }
            s_api.m_duration.Remove(clipName);
            s_api.m_duration.Add(clipName, Time.time);
            GameObject go = Instantiate(s_api.m_audioPrefab);
            AudioHelper source = go.GetComponent<AudioHelper>();
            source.Play(s_api.GetClip(clipName), loop);
            AudioSource audioComp = go.GetComponent<AudioSource>();
            audioComp.volume = s_api.m_soundOn ? 1 : 0;
            if (s_api.m_sounds == null)
                s_api.m_sounds = new List<AudioSource>();
            s_api.m_sounds.Add(audioComp);
            return go;
        }
    }
    public static GameObject PlaySound3D(string clipName, float distance, bool loop, Vector2 position)
    {
        if (!loop)
        {
            if (s_api.m_durationConfig.ContainsKey(clipName) && s_api.m_duration.ContainsKey(clipName))
            {
                float delTime = Time.time - s_api.m_duration[clipName];
                if (delTime < s_api.m_durationConfig[clipName])
                    return null;
            }
        }
        GameObject go = Instantiate(s_api.m_audioPrefab);
        AudioHelper source = go.GetComponent<AudioHelper>();
        source.Play3D(s_api.GetClip(clipName), distance, loop, position);
        AudioSource audioComp = go.GetComponent<AudioSource>();
        audioComp.volume = s_api.m_soundOn ? 1 : 0;
        if (s_api.m_sounds == null)
            s_api.m_sounds = new List<AudioSource>();
        s_api.m_sounds.Add(audioComp);
        return go;
    }

    public AudioClip GetClip(string clipName)
    {
        return m_clips.FirstOrDefault(t => t.name == clipName);
    }
    void OnApplicationQuit()
    {
        SaveCache();
    }

    public static void StopMusic()
    {
        if (s_api.m_music == null)
            return;
        Destroy(s_api.m_music.gameObject);
        s_api.m_music = null;
    }
}
