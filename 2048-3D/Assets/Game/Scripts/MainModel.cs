using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MainModel 
{
    public static int scoreLevel;
    public static int countCubelLevel;
    //
    public static int highCountCube;
    public static int highScore;
    public static int boom;
    public static int heart;
    //
    public static status statusGame;
  
    public static void LoadData()
    {
        scoreLevel = 0;
        countCubelLevel = 0;
        //
        highCountCube = PlayerPrefs.GetInt("HighCountCube", 0);
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        boom = PlayerPrefs.GetInt("Boom", 5);
        heart = PlayerPrefs.GetInt("Heart", 0);
    }
    public static void SaveData()
    {
        PlayerPrefs.SetInt("HighCountCube", highCountCube);
        PlayerPrefs.SetInt("HighScore", highScore);
        PlayerPrefs.SetInt("Boom", boom);
        PlayerPrefs.SetInt("Heart", heart);
        //
        PlayerPrefs.Save();
    }
    public static void UpdateScore(int valueScore)
    {
        countCubelLevel++;
        if (countCubelLevel > highCountCube)
            highCountCube = countCubelLevel;
        //
        scoreLevel += valueScore;
        if (scoreLevel > highScore)
            highScore = scoreLevel;
        //
        SaveData();
    }
    public static void UpdateHeart(int valueHeart)
    {
        heart += valueHeart;
        //
        SaveData();

    }
    public static void UpdateBoom(int valueBoom)
    {
        boom += valueBoom;
        //
        SaveData();
    }
    public static void ChangeStatusGame(status valueStatus)
    {
        statusGame = valueStatus;
    }
}
public enum status
{
    play,
    stop,
    gameOver
}
